$(document).ready(function () {
 tinymce.init({
        selector: "textarea",
        statusbar: false,
        setup: function (editor) {
            editor.on('change', function () {
                editor.save();
            });
        }
    });

        $("#ajaxform").submit(function (e)
        {
            var postData = $(this).serializeArray();
            console.log(postData);
            var formURL = $(this).attr("action");
            $.ajax(
                    {
                        url: formURL,
                        type: "POST",
                        data: postData,
                        beforeSend: function () {
                            //show laoding 
                            $('#loading_data').html('<img src="../../img/loading.gif"/>&nbsp Loading ....');
                        },
                        success: function (data, textStatus, jqXHR)
                        {
                            if (data == 'done') {
                                $('#loading_data').css('color', 'green');
                               $('#loading_data').html('data added successfully');
                            } else if (data.status == 'valid_error') {
                                $('#loading_data').html(data.fileds);
                                $('#loading_data').css('color', 'red');
                                $('#submit').removeAttr('disabled');
                            } else {
                                $('#loading_data').css('color', 'red');
                                $('#loading_data').html('Error In Process');
                            }
                   },
//                                                    alert('done'); },}
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            //if fails     
                        }
                    });
            e.preventDefault(); //STOP default action
        });
       

});


