$(document).ready(function (){
$("#form_theme_upload").submit(function (){
	$('#submit').attr("disabled", "true");
	file_data = new FormData($('#form_theme_upload')[0]);
	//upload image
	$.ajax({
			type: 'POST',
			url: 'data_model/uploading_theme.php',
			data: file_data,
			cache: false,
			contentType: false,
			processData: false,
			beforeSend : function (){
				$('#loading_data').html('<img src="../../img/loading.gif"/>&nbsp Uploading File ....');
			},
			success: function(upload){
				if(upload.status == "work"){
					var data = {	
						file_name: upload.name,
					};	
					//insert post data
					$.ajax({
						type: 'POST',
						url: 'data_model/insert.php',
						data: data,
						beforeSend: function(){
							$('#loading_data').html('<img src="../../img/loading.gif"/>&nbsp Loading Compressing File ....');
						},
						success: function(data){
							alert(data);
						}
					})					
				}else{
					$('#upload_file').val('');
					$('#submit').removeAttr("disabled");
					$('#loading_data').html(upload['message']);
				}
			}
		})	
  return false;     
 })    
})