$(document).ready(function (){
	var main_content= {};
$("#form_crud").submit(function (){
	 $('#submit').attr("disabled", "true");
	 var type = 'POST';
	 var url = $('#form_crud').attr('action');
	//get all main content values
	$(".main_content").each(function(){
		if($(this).is("textarea")){
			var textarea_value = tinymce.get($(this).attr("id")).getContent()
			main_content[$(this).attr("id")] = textarea_value; 
		}else{
			 main_content[$(this).attr("id")] = $(this).val(); 
		}
	});	 
	 //send json data
	 var data = {	
			task: $('#process_type').val(),
			record: $('#record').val(),
			main_content: main_content,
			icon: $('#icon').val(),
			group_id:$('#group_id').val(), 
			sid:$('#sid').val(),
			path_type:$('#path_type').val(),
			drop_dwon_style:$('input[name=drop_dwon_style]:checked').val(),
			drop_down:$('input[name=drop_down]:checked').val(),
			shadow:$('input[name=shadow]:checked').val(),
			path:$('#path').val(),
			sorting:$('#sorting').val(),
			imageVal:$('#imageVal').val(),
			external_path :$("#external_path").val()
		};
		
	$.ajax({
	 type: type,
	 url: url,
	 data: data,
	 beforeSend: function(){
		//show laoding 
		$('#loading_data').html('<img src="../../img/loading.gif"/>&nbsp Loading ....');
	 },
	 success: function(data){
		if(data.status == 'work'){
			if($('#process_type').val() == 'update'){
				window.location.href = "full_info.php?id="+$('#record').val();
			}else{
			//load menu in side par
			$('#sid').empty();
			$('#sid').load('data_model/reloud_select.php?menu_id='+data.group_id);
			$('#form_crud')[0].reset();
			$('#submit').removeAttr("disabled");
			$('#loading_data').html('');	
			$.ajax({
				type:"GET",
				url: 'data_model/view_menu_links.php?m_id='+$('#group_id').val()+"&lang_id="+$('#lang_id').val(),
				beforeSend: function(){
					//show laoding 
					$('#loading_data').html('');	
					$('#loading_data').html('Menu links <img src="../../img/loading.gif"/>&nbspLoading ....');
				},success: function(data){
					$('.module_page_view_list').html('');
					$('.module_page_view_list').html(data);
					$('#external_input').hide();
					$('#internal_input').show()
					$('#loading_data').html('Successful Insert Process');	
				}
			 });				 
			}
		}else if(data.status == 'valid_error'){
		 	$('#loading_data').html(data.fileds);
		 	$('#loading_data').css('color', 'red');
		 	$('#submit').removeAttr('disabled');	 
		}else{
			$('#loading_data').html('Error In Process');
		}
	 }
})
 //do not go to any where
  return false;     
 });
 $('.confirm_delete').click(function(){
	
	var task = $("#task_type").val();
	var del_id = $(this).attr('id');
	var data = {
		 task : task ,
		 id : del_id
		};
	var url = "data_model/delete.php";
	var type = "GET";
	$.ajax({
		url : url,
		type : type,
		data : data,
		success : function(data){
			$('#ml_'+del_id).remove();
		}
		
		
		});
	});
 

});