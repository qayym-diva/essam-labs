$(document).ready(function (){

$("#form_crud").submit(function (){
	 //disable button
	 $('#submit').attr("disabled", "true");
	 var type = 'POST';
	 var url = $('#form_crud').attr('action');
	 //send json data
	 var data = {	
			task: $('#process_type').val(),
			record: $('#record').val(),
			dir_id: $('#dir_id').val(), 
			file_name: $('#file_name').val(), 
			alt_text: $('#alt_text').val(), 
			description: $('#description').val(),
		};
	$.ajax({
	 type: type,
	 url: url,
	 data: data,
	 beforeSend: function(){
		//show laoding 
		$('#loading_data').html('<img src="../../img/loading.gif"/>&nbsp Loading ....');
	 },
	 success: function(data){
		if(data.status == 'work'){
			 if($('#process_type').val() == 'update'){
				 window.location.href = "view.php";
			 }else{
				$('#form_crud')[0].reset();
				$('#submit').removeAttr("disabled");
				$('#loading_data').html('Successful Insert Process');				 
			 }
		 }else if(data.status == 'exist'){
			 $('#submit').removeAttr("disabled");
			 $('#loading_data').html('Successful Insert, Cant Change File Name, The Name Is Already Exist');
		 }else{
			$('#loading_data').html('Error In Process');
		 }
	 }
})
 //do not go to any where
  return false;     
 })    
})