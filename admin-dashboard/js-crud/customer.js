$(document).ready(function (){
$("#form_crud").submit(function (){
	 //disable button
	 $('#submit').attr("disabled", "true");
	 var type = 'POST';
	 var url = $('#form_crud').attr('action');
	 //send json data
	 var data = {	
			task: $('#process_type').val(),
			record: $('#record').val(),
			mobile: $('#mobile').val(), 
			first_name: $('#first_name').val(), 
			last_name: $('#last_name').val(),
			email:  $('#email').val(),
			password: $('#password').val(),
			birthday:$("#birthday").val(),
			directions:$("#directions").val(),
			
		};
	$.ajax({
	 type: type,
	 url: url,
	 data: data,
	 beforeSend: function(){
		//show laoding 
		$('#loading_data').html('<img src="../../img/loading.gif"/>&nbsp Loading ....');
	 },
	 success: function(data){
		if(data.status == 'work'){
		 if($('#process_type').val() == 'update'){
			window.location.href = "full_info.php?id="+$('#record').val();
		 }else{
			window.location.href = "full_info.php?id="+data.inserted_id;
		 }
	 }else if(data.status == 'valid_error'){
		 $('#loading_data').html(data.fileds);
		 $('#loading_data').css('color', 'red');
		 $('#submit').removeAttr('disabled');
	 }else if(data.status == 'email_exist'){
		  $('#loading_data').html("- Email  exists");
		   $('#loading_data').css('color', 'red');
		 
	 }else if(data.status == 'email_error'){
		  $('#loading_data').html("- Email is   incorrect");
		   $('#loading_data').css('color', 'red');
		 
	 }else{
		$('#loading_data').html('Error In Process');
	 }
		
	 }
});
 //do not go to any where
  return false;     
 });    
});