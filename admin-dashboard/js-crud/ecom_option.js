$(document).ready(function (){
var inserted_values = [];
var main_content = {};
//delete selected tr
$('.delete_tr').live('click', function() {
  		var tr = $(this).closest('tr');
        tr.fadeOut(400, function(){
            tr.remove();
        });
});	
//add new tr
$('.add_tr').live('click', function() {
	var id_num = $('.option_values_tbl tr:last').attr("id");
	id_num++;
	$('.option_values_tbl tr:last').after("<tr class='option_values' id='"+id_num+"'><td><input type='text' class='form-control name' autocomplete='off'></td><td><input type='text' class='form-control sort' autocomplete='off'></td><td><a href='' class='btn btn-primary btn-xs tooltips add_tr' data-placement='top'><i class=' icon-plus-sign-alt'></i></a>&nbsp<a href='#' data-toggle='modal' class='btn btn-danger btn-xs tooltips delete_tr' data-placement='top' data-original-title='Delete'>  <i class='icon-remove'></i></a></td></tr>");
	return false;
});	

$("#form_crud").submit(function (){
	//disable button
	$('#submit').attr("disabled", "true");
	var type = 'POST';
	var url = $('#form_crud').attr('action');
	
	//get all main content values
	$(".main_content").each(function(){
		if($(this).is("textarea")){
			var textarea_value = tinymce.get($(this).attr("id")).getContent()
			main_content[$(this).attr("id")] = textarea_value; 
		}else{
			 main_content[$(this).attr("id")] = $(this).val(); 
		}
	});	 	
		
	//get values
	$('.option_values').each(function() { 
		 inserted_values[$(this).attr("id")] = $(this).find('input.name').val()+','+$(this).find('input.sort').val();
	})	
 	
	 //send json data
	 var data = {	
			task: $('#process_type').val(),
			record: $('#record').val(),
			view_type:$('input[name=view_type]:checked').val(),
			view_style:$('input[name=view_style]:checked').val(),
			main_content: main_content,
			inserted_values:inserted_values
		};
	$.ajax({
	 type: type,
	 url: url,
	 data: data,
	 beforeSend: function(){
		//show laoding 
		$('#loading_data').html('<img src="../../img/loading.gif"/>&nbsp Loading ....');
	 },
	 success: function(data){
		if(data.status == 'work'){
			 if($('#process_type').val() == 'update'){
				window.location.href = "full_info.php?id="+$('#record').val();
			 }else{
				window.location.href = "full_info.php?id="+data.inserted_id;
			 }
		}else if(data.status == 'valid_error'){
		 	$('#loading_data').html(data.fileds);
		 	$('#loading_data').css('color', 'red');
		 	$('#submit').removeAttr('disabled');
		}else{
			$('#loading_data').html('Error In Process');
		}
	 }
	})
 //do not go to any where
  return false;     
 })   
  
})
