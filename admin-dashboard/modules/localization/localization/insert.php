<?php 
require_once("../layout/initialize.php"); 
require_once("../layout/header.php"); 
?> 
<script type="text/javascript" src="../../js-crud/crud_localization.js"></script> 
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start--> 
  <section id="main-content"> 
      <section class="wrapper site-min-height"> 
      <h4> Localization Module</h4> 
       <div class="row"> 
          <aside class="profile-info col-lg-9"> 
            <section> 
              <div class="panel "> 
                <div class="panel-heading"> Add Languages</div> 
                <div class="panel-body"> 
               <form role="form" action="data_model/insert.php" id="form_crud"> 
                  <input type="hidden" id="process_type" value="insert"> 
                  <div class="form-group"> 
                      <label for="col-lg-2">Name:</label> 
                      <input type="text" class="form-control" id="name" placeholder=""> 
                  </div> 
                  <div class="form-group"> 
                      <label for="col-lg-2">Label</label> 
                      <input type="text" class="form-control" id="label" placeholder=""> 
                  </div> 
                  <button type="submit" id="submit" class="btn btn-info">Submit</button> 
                   <div id="loading_data"></div> 
               </form> 
              </div> 
             </div> 
           </section> 
        </aside> 
       </div> 
       <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
  
  <?php require_once("../layout/footer.php");?>