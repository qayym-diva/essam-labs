<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/MenuGroup.php'); 
require_once('../../../../classes/Users.php'); 
require_once('../../../../classes/Profile.php'); 
//check log in  
if($session->is_logged() == false){ 
	redirect_to("../../index.php"); 
} 
//send json data 
header('Content-Type: application/json'); 
// user log in profile details to chech authority 
// get user profile   
$user_data = Users::find_by_id($session->user_id); 
// get user profile data 
$user_profile  = Profile::Find_by_id($user_data->user_profile); 
//send notifiction by json  
header('Content-Type: application/json'); 
if(!empty($_POST["task"]) && $_POST["task"] == "update"){ 
	//get data 
	$id = $_POST['record']; 
	$edit = MenuGroup::find_by_id($id); 
	 if($user_profile->global_edit != 'all_records' && $edit->inserted_by != $session->user_id  ){	 
		   redirect_to("../view.php"); 
	 }else{ 
			//validite required required 
		  $required_fields = array('title'=>"- Insert Title", 'alias'=>"- Insert Alias"); 
		  $check_required_fields = check_required_fields($required_fields); 
		  if(count($check_required_fields) == 0){ 
				//update 
				$edit->title = $_POST["title"]; 
				$edit->alias = $_POST["alias"]; 
				$edit->description = $_POST["description"]; 
				$edit->update_by=$session->user_id; 
				$edit->last_update = date_now();	 
				 if(!empty($_POST['imageVal'])){ 
					  $current_file = $_POST['imageVal']; 
					  $parts = explode('/',$current_file); 
					  $image_cover = $parts[count($parts)-1]; 
					  $edit->image = $image_cover; 
				   } 
				$update = $edit->update(); 
				if($update){ 
					$data  = array("status"=>"work"); 
					echo json_encode($data); 
				}else{ 
					$data  = array("status"=>"error"); 
					echo json_encode($data); 
				} 
		  }else{ 
				//validation erro 
				$comma_separated = implode("<br>", $check_required_fields); 
				$data  = array("status"=>"valid_error", "fileds"=>$comma_separated); 
				echo json_encode($data); 
			}	 
		} 
  } 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>