<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/MenuGroup.php'); 
//check log in  
if($session->is_logged() == false){ 
	redirect_to("../../index.php"); 
} 
//send notifiction by json  
header('Content-Type: application/json'); 
if(!empty($_POST["task"]) && $_POST["task"] == "insert"){ 
	//validite required required 
   $required_fields = array('title'=>"- Insert Title", 'alias'=>"- Insert Alias"); 
   $check_required_fields = check_required_fields($required_fields); 
	 if(count($check_required_fields) == 0){ 
		  $add = new MenuGroup(); 
		  $add->title = $_POST["title"]; 
		  $add->alias = $_POST["alias"]; 
		  $add->description = $_POST["description"]; 
		  $add->inserted_by=$session->user_id; 
		  $add->inserted_date = date_now(); 
		  if(!empty($_POST['imageVal'])){ 
			  $current_file = $_POST['imageVal']; 
			  $parts = explode('/',$current_file); 
			  $image_cover = $parts[count($parts)-1]; 
			  $add->image = $image_cover; 
	     } 
		  $insert = $add->insert(); 
		  $inserted_menu_group_id = $add->id; 
		  if($insert){			   
			  $data  = array("status"=>"work"); 
			  echo json_encode($data); 
		  }else{ 
			  $data  = array("status"=>"error"); 
			  echo json_encode($data); 
		  } 
	 }else{ 
		//validation error 
		$comma_separated = implode("<br>", $check_required_fields); 
		$data  = array("status"=>"valid_error", "fileds"=>$comma_separated); 
		echo json_encode($data); 
	}		 
} 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>