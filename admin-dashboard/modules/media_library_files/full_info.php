<?php 
	require_once("../layout/initialize.php"); 
	//get directory data 
	$directory_id = $_GET['dir_id']; 
	$get_directory_data = MediaLibraryDirectories::find_by_id($directory_id); 
	//get file data 
	$file_id = $_GET["id"]; 
	$define_class = new MediaLibraryFiles();	 
	$record_info = $define_class->media_library_files_data(null,null,$file_id); 
	require_once("../layout/header.php"); 
?> 
  <script type="text/javascript" src="../../js-crud/crud_media_file.js"></script>  
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start--> 
  <section id="main-content"> 
    <section class="wrapper site-min-height"> 
      <h4>Media Library Module</h4> 
      <!-- page start--> 
      <div class="row"> 
        <aside class="profile-info col-lg-8"> 
          <section> 
            <div class="panel"> 
              <div class="panel-heading"> Full Info</div> 
              <div class="panel-body"> 
                <form class="form-horizontal tasi-form" role="form"> 
                  <div class="form-group"> 
                    <label  class="col-lg-3">File Name:</label> 
                    <div class="col-lg-8"> 
                     <?php echo $record_info->name?> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-2">Source File:</label> 
                    <div class="col-lg-8"> <?php echo "<img src='../../../media-library/{$get_directory_data->title}/{$record_info->name}.{$record_info->type}' style='width:90%; height:80%;'>";?></div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-2">Alternative Text:</label> 
                    <div class="col-lg-8"> 
                      <?php echo $record_info->alternative_text?> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-2">Description:</label> 
                    <div class="col-lg-8"> 
                     <?php echo html_entity_decode($record_info->description)?> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                  <div class="col-lg-offset-2 col-lg-10"> 
                    <button type="button" class="btn btn-info"   
                        onClick="window.location.href = 'update.php?id='+<?php echo $file_id?>+'&dir_id='+<?php echo $directory_id?>" >Update </button> 
                        </div> 
                  </div> 
                </form> 
              </div> 
            </div> 
          </section> 
        </aside> 
        <div class="col-lg-3"> 
          <section class="panel panel-primary"> 
            <header class="panel-heading"> File Information: </header> 
            <div class="panel-body"> 
              <div id="list_info"> 
                <ul> 
                  <li><span style="color:#428bca">> File Name:</span> <?php echo $record_info->name.".".$record_info->type?></li> 
                  <li><span style="color:#428bca">> File Type:</span> <?php echo $record_info->type?></li> 
                  <li><span style="color:#428bca">> File Size:</span> <?php echo formatBytes($record_info->size)?></li> 
                  <li><span style="color:#428bca">> Created By:</span> <?php echo $record_info->inserted_by?></li> 
                  <li><span style="color:#428bca">> Created Date:</span> <?php echo $record_info->inserted_date?></li> 
                  <li><span style="color:#428bca">> Last Update By:</span> 
                    <?php if($record_info->updated_by!=""){ 
					  echo $record_info->update_by; 
				      }else{ 
						  echo "--";} 
					  ?> 
                  </li> 
                  <li><span style="color:#428bca">> Last Update Date:</span> 
                    <?php if($record_info->last_update!="0000-00-00 00:00:00"){ 
					  echo $record_info->last_update; 
				      }else{ 
						  echo "--";} 
					  ?> 
                  </li> 
                 
                </ul> 
              </div> 
            </div> 
          </section> 
        </div> 
      </div> 
      <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
  <?php require_once("../layout/footer.php");?>