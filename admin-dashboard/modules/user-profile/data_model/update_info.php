<?php 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/Users.php'); 
require_once('../../../../classes/Session.php'); 
if(!empty($_POST["task"]) && $_POST["task"] == "update"){ 
	header('Content-Type: application/json');		 
	//get data 
	$id = $session->user_id; 
	//update	 
	$edit = Users::find_by_id($id);		 
	$edit->first_name = $_POST["first_name"]; 
	$edit->last_name = $_POST["last_name"]; 
	$edit->update_by = $session->user_id; 
	$edit->last_update = date_now();	 
	//if email changed 
	if($edit->email != $_POST['email']){ 
		$email = $_POST['email']; 
		//check if email already taken 
		$check_email = Users::find_by_custom_filed('email', $email); 
		if(!$check_email){ 
			$edit->email = $_POST['email']; 
			//perform update 
			$update = $edit->update(); 
			if($update){ 
				$data  = array("status"=>"work"); 
				echo json_encode($data); 
			}else{ 
				$data  = array("status"=>"error"); 
				echo json_encode($data); 
			} 
		}else{ 
			$data  = array("status"=>"email_exist"); 
			echo json_encode($data);					 
		} 
	}else{ 
		//if email not changed preform update 
		$update = $edit->update(); 
		if($update){ 
			$data  = array("status"=>"work"); 
			echo json_encode($data); 
		}else{ 
			$data  = array("status"=>"error"); 
			echo json_encode($data); 
		} 
	} 
} 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?> 
