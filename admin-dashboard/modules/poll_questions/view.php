<?php  
	require_once("../layout/initialize.php"); 
	$define_class = new PollQuestions(); 
	//$define_class->enable_relation(); 
	$records =$define_class->poll_questions_data("inserted_date","DESC"); 
	require_once("../layout/header.php"); 
?> 
<!--header end--> 
<!--sidebar start--> 
<?php require_once("../layout/navigation.php");?> 
<!--sidebar end-->  
<!--main content start--> 
<section id="main-content"> 
  <section class="wrapper site-min-height"> 
    <h4> Social POll   Module</h4> 
    <div class="row"> 
      <div class="col-lg-12"> 
        <section class="panel"> 
          <header class="panel-heading"> Show  All Polls </header> 
          <br> 
          <button type="button" class="btn btn-danger" style="margin-left:15px" onClick="window.location.href = 'insert.php'" <?php 
		  $module_name = $opened_url_parts[count($opened_url_parts) - 2]; 
		  $opened_module_page_insert = $module_name.'/insert'; 
		  if(!in_array($opened_module_page_insert, $user_allowed_page_array)){ 
			echo "disabled"; 
		  } 
		  ?>> 
          <li class="icon-plus-sign"></li> 
          Add New Poll </button> 
          <br/> 
          <br/> 
          <table class="table table-striped table-advance table-hover"> 
            <thead> 
              <tr> 
                <th>#</th> 
                <th><i class=""></i> Poll</th> 
                <th><i class=""></i> Status</th> 
                <th><i class=""></i> created Date</th> 
                <th><i class=""></i> created by</th> 
                <th>Poll Option</th> 
                <th >Action</th> 
              </tr> 
            </thead> 
            <tbody> 
              <?php  
				  $serialize = 1;  
				  foreach($records as $record){ 
					echo "<tr> 
					   <td>{$serialize}</td> 
					   <td><a href='full_info.php?id={$record->id}'>{$record->poll}</a></td> 
					   <td>{$record->status}</td> 
					   <td>{$record->inserted_date}</td> 
					   <td>{$record->inserted_by}</td> 
					   <td>"; 
					$opened_module_view_answers = 'poll_questions_options/view'; 
					$opened_module_insert_question =  'poll_questions_options/insert'; 
					if(!in_array($opened_module_view_answers, $user_allowed_page_array)){ 
						echo " <a href='' class='btn btn-success btn-xs tooltips'  
					   data-placement='top' data-toggle='tooltip' data-original-title='View menu link' disabled><i class='icon-question-sign'></i></a>";	 
					}else{ 
						echo " <a href='../poll_questions_options/view.php?poll_id={$record->id}' data-toggle='modal' class='btn btn-success btn-xs tooltips' 
						 data-placement='top' data-original-title='View Answers'><i class='icon-question-sign'></i></a> ";	 
					} 
					if(!in_array($opened_module_insert_question, $user_allowed_page_array)){ 
						echo " <a href='' class='btn btn-success btn-xs tooltips' data-placement='top' data-toggle='tooltip' data-original-title='View menu link' 
						 disabled><i class='icon-plus-sign-alt'></i></a>";	 
					}else{ 
						echo "<a href='../poll_questions_options/insert.php?poll_id={$record->id}' data-toggle='modal' class='btn btn-success btn-xs tooltips' 
						 data-placement='top' data-original-title='Add New Options'><i class='icon-plus-sign-alt'></i></a>"; 
					} 
					   echo "</td> 
					   <td>"; 
						$module_name = $opened_url_parts[count($opened_url_parts) - 2]; 
						include('../layout/btn_control.php'); 
					echo	"</td> 
					</tr>";   //delete dialoge  
			 echo   "<div class='modal fade' id='my{$record->id}' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'> 
											<div class='modal-dialog'> 
												<div class='modal-content'> 
													<div class='modal-header'> 
														<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button> 
														<h4 class='modal-title'>Delete</h4> 
													</div> 
													<div class='modal-body'> 
													 <p> Are you sure you want delete  $record->poll??</p> 
													</div> 
													<div class='modal-footer'> 
														<button class='btn btn-warning' type='button'  
														onClick=\"window.location.href = 'data_model/delete.php?task=delete&id={$record->id}'\"/> Confirm</button>  <button data-dismiss='modal' class='btn btn-default' type='button'>cancle</button> 
													</div> 
												</div> 
											</div> 
										</div>"; 
								 $serialize++; 
							   }?> 
            </tbody> 
          </table> 
        </section> 
      </div> 
    </div> 
    <!-- page end-->  
  </section> 
</section> 
<!--main content end-->  
<!--footer start--> 
<?php require_once("../layout/footer.php");?>