<?php
require_once('../../../../classes/Session.php');
require_once('../../../../classes/Functions.php');
require_once('../../../../classes/MysqlDatabase.php');
require_once('../../../../classes/Forms.php');
require_once('../../../../classes/Users.php');
require_once('../../../../classes/Profile.php');
//check  session user  log in
if($session->is_logged() == false){
	redirect_to("../../../index.php");
}
// get user profile  
$user_data = Users::find_by_id($session->user_id);
// get user profile data
$user_profile  = Profile::Find_by_id($user_data->user_profile);
// check if the user profile block
if($user_profile->profile_block == "yes"){
   redirect_to("../../../index.php");	
}
 $path = "../../../../forms/";
if(!empty($_POST["task"]) && $_POST["task"] == "update"){
	//get data
	 $id = $_POST["record"];
	 $edit = Forms::find_by_id($id);
	 $old_name = $path.$edit->label;
	 $label = $_POST["label"];
	 $new_name = $path.$label;
	 
	 
	if($user_profile->globel_edit != 'all_records' && $edit->inserted_by != $session->user_id  ){	
		   redirect_to("../view.php");
	}else{
		
		  header('Content-Type: application/json');
		  //validite required required
		  $required_fields = array('name'=>"- Insert Name", 'label'=>'- Insert label');
		  $check_required_fields = check_required_fields($required_fields);
		  if(count($check_required_fields) == 0){
			//update  
		 //check if form label is valid 
		      if(strpbrk($label, "\\/?%*:|\"<>") === false){
				      $edit->name = $_POST["name"];
					   $edit->email_to = $_POST["email"];
					  $edit->enable = $_POST["enable"];
					  $edit->label = $_POST["label"];
					  $edit->update_by = $session->user_id;
					  $edit->last_update = date_now();
					  rename($old_name,$new_name);
					  $update = $edit->update();
					  if($update){
						  
						  $data  = array("status"=>"work","id"=>$id);
						  echo json_encode($data);
					  }else{
						  $data  = array("status"=>"error");
						  echo json_encode($data);
					  }
			 }else{
				 //if label not valid
				 $data  = array("status"=>"wrong");
				 echo json_encode($data);
			}
					
		   
		}else{
			  //validation error
			  $comma_separated = implode("<br>", $check_required_fields);
			  $data  = array("status"=>"valid_error", "fileds"=>$comma_separated);
			  echo json_encode($data);
		}
  }	
}
//close connection
if(isset($database)){
	$database->close_connection();
}

?>