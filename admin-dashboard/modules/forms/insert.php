<?php 
require_once("../layout/initialize.php");
require_once("../layout/header.php");
include("../../assets/texteditor4/head.php"); ?>
<script type="text/javascript" src="../../js-crud/crud_form.js"></script>
  <!--header end--> 
  <!--sidebar start-->
  <?php require_once("../layout/navigation.php");?>
  <!--sidebar end--> 
  <!--main content start-->
  <section id="main-content">
    <section class="wrapper site-min-height">
      <h4>Forms</h4>
      <!-- page start-->
      <div class="row">
        <aside class="col-lg-10">
          <section>
            <div class="panel">
              <div class="panel-heading"> Add  Form</div>
              <div class="panel-body">
                 <form class="form-horizontal tasi-form" role="form" action="data_model/insert.php"  id="form_crud">
                  <input type="hidden" id="process_type" value="insert">
                   <div class="form-group">
                     <label for="" class="col-lg-2 ">Name:</label>
                      <div class="col-lg-8">
                        <input type="text" class="form-control" id="name">
                       </div>
                     </div>
                     <div class="form-group">
                     <label for="" class="col-lg-2 ">Label:</label>
                      <div class="col-lg-8">
                        <input type="text" class="form-control" id="label">
                       </div>
                     </div>
                     <div class="form-group">
                     <label for="" class="col-lg-2 ">Email:</label>
                      <div class="col-lg-8">
                        <input type="text" class="form-control" id="email">
                       </div>
                     </div>
                     <div class="form-group">
                        <label class="col-lg-2">Enable:</label>
                        <div class="col-lg-8">
                          <label class="checkbox-inline">
                            <input type="radio" name="enable" class="radio" value="yes" >
                            Yes</label>
                          <label class="checkbox-inline">
                            <input type="radio" name="enable" class="radio" value="no" checked>
                            No</label>
                        </div>
                      </div>
                    
                        
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-6">
                            <button type="submit" id="submit" class="btn btn-info">Save</button>
                             <button type="submit" class="btn btn-default">Cancel</button>
                             <div id="loading_data"></div
                        ></div>
                    </div>
            </form>
                             
              </div>
            </div>
          </section>
        </aside>
        
      </div>
      <!-- page end--> 
    </section>
  </section>
  <!--main content end--> 
  <!--footer start-->
  
  <?php require_once("../layout/footer.php");?>