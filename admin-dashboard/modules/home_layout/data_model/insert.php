<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/Users.php'); 
require_once('../../../../classes/Profile.php'); 
require_once('../../../../classes/HomePageLayout.php'); 
//check  session user  log in 
if($session->is_logged() == false){ 
	redirect_to("../../../index.php"); 
} 
// get user profile   
$user_data = Users::find_by_id($session->user_id); 
// get user profile data 
$user_profile  = Profile::Find_by_id($user_data->user_profile); 
// check if the user profile block 
if($user_profile->profile_block == "yes"){ 
   redirect_to("../../../index.php");	 
} 
//send notifiction by json 
header('Content-Type: application/json');	 
if(!empty($_POST["task"]) && $_POST["task"] == "insert"){ 
	//validite required required 
	$required_fields = array('postion'=>'- Select Postion',"plugin"=>"- Select Plugin "); 
    $check_required_fields = check_required_fields($required_fields); 
    if(count($check_required_fields) == 0){ 
			//insert data 
			$add = new HomePageLayout(); 
			$add->position = $_POST["postion"] ; 
			$add->status = $_POST["status"]; 
		    $add->title = $_POST["title"]; 
			$add->plugin = $_POST["plugin"]; 
			$add->header_title = $_POST["header_title"];
			$add->plugin_value = $_POST["plugin_value"];
		     $insert = $add->insert(); 
			$inserted_id = $add->id; 
			if($insert){ 
				  //return json  
				$data  = array("status"=>"work"); 
				echo json_encode($data); 
			}else{ 
				$data  = array("status"=>"error"); 
				echo json_encode($data); 
			} 
 	 }else{ 
		  //validation error 
		  $comma_separated = implode("<br>", $check_required_fields); 
		  $data  = array("status"=>"valid_error", "fileds"=>$comma_separated); 
		  echo json_encode($data); 
	  } 
} 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>