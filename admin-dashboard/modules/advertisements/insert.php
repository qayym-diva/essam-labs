<?php
	require_once("../layout/initialize.php");
	require_once("../layout/header.php");
	include("../../assets/texteditor4/head.php");  
	$translate_lang = new GeneralSettings();
	$translate_lang->enable_relation();
	$translate_lang_data = $translate_lang->general_settings_data();
?>
<script type="text/javascript" src="../../js-crud/adv.js"></script>
<!--header end-->
<!--sidebar start-->
<?php require_once("../layout/navigation.php");?>
<!--sidebar end--> 
<!--main content start-->
<section id="main-content">
  <section class="wrapper site-min-height">
    <h4> Advertisements  Module</h4>
    <!-- page start-->
    <div class="row">
      <aside class="col-lg-8">
        <section>
          <div class="panel">
            <div class="panel-heading"> Add Advertisement</div>
            <div class="panel-body">
              <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/insert.php"> 
                <input type="hidden" id="process_type" value="insert"> 
                <section class="panel"> 
                  <header class="panel-heading tab-bg-dark-navy-blue"> 
                    <ul class="nav nav-tabs"> 
                      <li class=" center-block active" > <a data-toggle="tab" href="#main_info" class="text-center"><strong>Main Info</strong></a></li> 
                      <li class=" center-block"> <a data-toggle="tab" href="#adv_option" class="text-center"><strong> Advertisement  Option</strong></a></li> 
                    </ul> 
                  </header> 
                  <div class="panel-body"> 
                    <div class="tab-content"> 
                      <div id="main_info" class="tab-pane active "> 
                        <section class="panel col-lg-9"> 
                          <header class="panel-heading tab-bg-dark-navy-blue "> 
                            <ul class="nav nav-tabs"> 
                              <?php 
                            //create tabs for all available languages  
                            $languages = Localization::find_all('id','desc'); 
                            $serial_tabs = 1; 
                            foreach($languages as $language){ 
                                $lang_tab_header = ucfirst($language->name); 
                                echo "<li class='";if($serial_tabs == 1){ echo " active ";}  echo"'> <a data-toggle='tab' href='#$language->name'> 
                                <strong>$lang_tab_header</strong></a></li>"; 
                                $serial_tabs++; 
                            } 
                          ?> 
                            </ul> 
                          </header> 
                          <div class="panel-body"> 
                            <div class="tab-content"> 
                              <?php 
                            $serial_tabs_content = 1; 
                            foreach($languages as $language){ 
                                    echo " 
                                    <div id='$language->name' class='tab-pane"; if($serial_tabs_content == 1){ echo " active ";} echo"'> 
                                            <div class='form-group'> 
                                            <label  class='col-lg-2'>Title:</label> 
                                            <div class='col-lg-9'> 
                                              <input type='text' class='form-control main_content' id='title_$language->label' autocomplete='off'> 
                                            </div> 
                                          </div> 
                                          <div class='form-group'> 
                                            <label class='col-lg-2'>Content:</label> 
                                            <div class='col-md-9'> 
                                              <textarea class='form-control main_content' id='content_$language->label'></textarea> 
                                            </div> 
                                          </div> 
                                    </div>"; 
                                $serial_tabs_content++; 
                            } 
                          ?> 
                            </div> 
                          </div> 
                        </section> 
                      </div> 
                      <div id="adv_option" class="tab-pane "> 
                        
                        <div class="form-group"> 
                          <label  class="col-lg-2 "> Path Type:</label> 
                          <div class="col-lg-8"> 
                            <select class="form-control" id="path_type"> 
                              <option value=""> Select Type </option> 
                              <option value="page">Page</option> 
                              <option value="post">Post</option> 
                              <option value="event">Event</option> 
                              <option value="product">Product</option> 
                            </select> 
                          </div> 
                        </div> 
                        <div class="form-group"  > 
                          <label  class="col-lg-2 "> Path:</label> 
                          <div class="col-lg-8" > 
                             <select class="form-control" id="path"> 
                              <option value=""> Select Node </option> 
                            </select> 
                          </div> 
                          <div id="node_loading_data"></div> 
                        </div> 
                      </div> 
                    </div> 
                  </div> 
                </section> 
                <div class="form-group"> 
                  <div class="col-lg-offset-2 col-lg-10"> 
                    <button type="submit" class="btn btn-info" id="submit">Save</button> 
                    <button type="submit" class="btn btn-default">Cancel</button> 
                    <div id="loading_data"></div> 
                  </div> 
                </div> 
              </form>
            </div>
          </div>
        </section>
      </aside>
      <div class="col-lg-4"> 
        <section class="panel panel-primary"> 
          <header class="panel-heading"> Publish Options: </header> 
          <div class="panel-body"> 
            <form class="form-horizontal tasi-form" role="form" id="form_option"> 
              <div class="form-group"> 
                <label class="col-lg-5">Status:</label> 
                <div class="col-lg-6"> 
                  <label class="checkbox-inline"> 
                    <input type="radio" name="status" class="radio" value="active"> 
                    Yes</label> 
                  <label class="checkbox-inline"> 
                    <input type="radio" name="status" class="radio" value="disable" checked> 
                    No</label> 
                </div> 
              </div> 
             </form> 
          </div> 
        </section> 
        <section class="panel panel-primary"> 
          <header class="panel-heading"> Images: </header> 
          <div class="panel-body"> 
            <form class="form-horizontal tasi-form" role="form" id="form_option"> 
              <div class="form-group">
                  <label  class="col-lg-5 ">Image Cover(Only png,jpg):</label>
                  <div class="col-lg-6"> <a href="../media_filemanager_adv/view_media_directories.php" id="image_cover">Select Image</a> <br />
                    <br />
                    <input type="hidden" class="form-control" id="imageVal" placeholder=" " autocomplete="off">
                    <div style="display:none" id="imageShow"> <img src="" id="imageSrc" style="width:80px; height:80px;"></div>
                  </div>
                </div> 
             </form> 
          </div> 
        </section>
      </div>
      
      
    </div>
    <!-- page end--> 
  </section>
</section>
<!--main content end--> 
<!--footer start-->
<?php require_once("../layout/footer.php");?>
<script src="../../js-crud/load_node_for_adv.js"></script>