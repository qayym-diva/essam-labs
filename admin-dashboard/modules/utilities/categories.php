<?php  
require_once("../layout/light_box_header.php"); 
require_once("../../../classes/Taxonomies.php"); 
//get all auhtor and category 
$define_class = new Taxonomies(); 
$define_class->enable_relation(); 
?> 
<script> 
$(document).ready(function (){ 
	//get current selected categories & checked it 
	parent.$(".selected_category li" ).each(function() { 
		var li_id = $(this).attr("id"); 
		$('.category_'+li_id).prop("checked",true); 
	}); 
	//when user submit get selected categories 
	//print selected categories in li view 
	$("#form_categories").submit(function (){	
	  
		parent.$(".selected_category").html(''); 
		$('input:checkbox[name="category[]"]:checked').each(function(){ 
			var category_id = $(this).attr("id"); 
			var category_name = $(this).val(); 
			parent.$(".selected_category"). 
			append('<li id="'+category_id+'">- '+category_name +'&nbsp<a href="#" class="DeleteCategory glyphicon glyphicon-remove"></a></li>'); 
			$('#feature_loading').html('The items were added successfully'); 
		
			
		});	 
		return false; 
	});	 
}); 
</script> 
<br> 
<div class="col-lg-6"> 
  <section class="panel"> 
  
    <header class="panel-heading"> <strong>All Categories:</strong> </header> 
    <div class="panel-body"> 
   
      <form id="form_categories" >
      <div class="form-group "> 
            <label  class="col-lg-2">Search:</label> 
            <div class="col-lg-3"> 
              <input type="text" class="form-control" id="search_input" placeholder=" " autocomplete="off" style="width:250px" /> 
            </div> 
          </div> 
         <div class="form-group" > 
        
          <div class="col-lg-8"> 
            <div class="checkboxes"> 
            <ul id="search_list">
              <?php  
			  $categories = $define_class->get_type(0,0,$main_lang_id,"category");
			  if($categories){
			    foreach ($categories as $key=>$value) { 
				  echo "<li><label class='label_check'><input type='checkbox' class='category_$key' id='{$key}' 
			        value='{$value}' name='category[]' >{$value}</label></li> ";
			    } 
			  }
			?> 
            </ul>
            </div> 
          </div> 
        </div> 
        <div class="form-group" > 
          <div class="col-lg-8"> 
            <button type="submit" class="btn btn-info">Select</button> 
            <div id="feature_loading"></div> 
          </div> 
        </div> 
      </form> 
    </div> 
  </section> 
</div> 
<?php require_once("../layout/light_box_footer.php");?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="../../js/jquery.fastLiveFilter.js"></script> 
<script>
    $(function() {
        $('#search_input').fastLiveFilter('#search_list');
    });
</script>