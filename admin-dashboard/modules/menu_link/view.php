<?php 
	require_once("../layout/initialize.php"); 
	if(isset($_GET['group_id']) && is_numeric($_GET["group_id"])){ 
		$group_id = $_GET["group_id"]; 
		$records = new MenuLink(); 
		$group_data = MenuGroup::find_by_id($group_id); 
		if(empty($group_data->id)){ 
			redirect_to('../menu_group/view.php'); 
		} 
	}else{ 
		redirect_to('../menu_group/view.php'); 
	} 
	require_once("../layout/header.php");	 
?> 
<script src="../../js-crud/menu_links.js"></script>
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  
  <!--sidebar end-->  
  <!--main content start--> 
  <section id="main-content"> 
    <section class="wrapper site-min-height"> 
      <h4>Menu link Module </h4> 
      <!-- page start--> 
    <section class="panel"> 
            <header class="panel-heading"> Show "<?php echo $group_data->title?>" Links </header> 
            <br/> 
            <button type="button" class="btn btn-danger" style="margin-left:15px" onClick="window.location.href = 'insert.php?group_id='+<?php echo $group_id ?>"  
			   <?php 
				  $module_name = $opened_url_parts[count($opened_url_parts) - 2]; 
				  $opened_module_page_insert = $module_name.'/insert'; 
				  if(!in_array($opened_module_page_insert, $user_allowed_page_array)){ 
					echo "disabled"; 
				  } 
				  ?>> 
            <li class="icon-plus-sign"></li> 
            Add New Menu  link </button> 
            <br> 
            <br/> 
            <div class="panel-body"> 
            <table class="table table-striped table-advance table-hover"> 
              <thead> 
                <tr> 
                  <th><i class=""></i> Title</th> 
                  <th class=""><i class=""></i> Created Date </th> 
                  <th>Action</th> 
                </tr> 
              </thead> 
              <tbody> 
              <?php  
			 $records->enable_relation(); 
			 $records->user_allowed_page_array = $user_allowed_page_array; 
			 $records->view_menu_links(0,$group_id,$general_setting_info->translate_lang_id,$user_profile->global_edit, 
			 $user_profile->global_delete,$user_data->id) 
			 ;?> 
              </tbody> 
            </table> 
            </div> 
          </section> 
         
      </div> 
      <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
  <?php require_once("../layout/footer.php");?>