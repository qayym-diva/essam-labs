<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/Users.php'); 
require_once('../../../../classes/Profile.php'); 
require_once('../../../../classes/MenuLink.php'); 
require_once('../../../../classes/MenuLinkContent.php'); 
require_once('../../../../classes/Localization.php'); 
//check  session user  log in 
if($session->is_logged() == false){ 
	redirect_to("../../../index.php"); 
} 
// get user profile   
$user_data = Users::find_by_id($session->user_id); 
// get user profile data 
$user_profile  = Profile::Find_by_id($user_data->user_profile); 
// check if the user profile block 
if($user_profile->profile_block == "yes"){ 
   redirect_to("../../../index.php");	 
} 
//send notifiction by json  
header('Content-Type: application/json'); 
if(!empty($_POST["task"]) && $_POST["task"] == "insert"){ 
//retrieve all available languages 
$languages = Localization::find_all('label','asc');	 
//validite required required 
	$add = new MenuLink(); 
	$add->icon= $_POST["icon"]; 
	if(!empty($_POST['imageVal'])){ 
		$current_file = $_POST['imageVal']; 
		$parts = explode('/',$current_file); 
		$image_cover = $parts[count($parts)-1]; 
		$folder = $parts[count($parts)-2]; 
		$path = $folder."/".$image_cover; 
		$add->image = $path; 
   } 
   if($_POST["path_type"] == "external"){ 
	 $add->external_path = $_POST["external_path"]; 
   }else{ 
	   $add->external_path = ""; 
   } 
	$add->group_id = $_POST["group_id"]; 
	$add->status = $_POST["shadow"]; 
	if(!empty($_POST["drop_down"])){
	  $add->drop_down_style = $_POST["drop_dwon_style"]; 
	   $add->drop_down = $_POST['drop_down'];
	}
	$add->parent_id = $_POST["sid"]; 
	$add->path_type = $_POST["path_type"]; 
	$add->path = $_POST["path"]; 
	$add->inserted_by = $session->user_id; 
	$add->sorting = $_POST["sorting"]; 
	$add->inserted_date = date_now(); 
	$insert = $add->insert(); 
	$inserted_id = $add->id; 
	if($insert){ 
		//insert link content  
		foreach($languages as $language){ 
			$add_content = new MenuLinkContent(); 
			$add_content->link_id = $inserted_id; 
			$add_content->lang_id = $language->id; 
			$add_content->title = $_POST['main_content']['title_'.$language->label]; 
			$add_content->description = $_POST['main_content']['description_'.$language->label]; 
			$add_content->insert(); 
		}			 
		$data  = array("status"=>"work","group_id"=>$add->group_id,"lang"=>$add_content->lang_id); 
		echo json_encode($data); 
	}else{ 
		$data  = array("status"=>"error"); 
		echo json_encode($data); 
	} 
} 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>