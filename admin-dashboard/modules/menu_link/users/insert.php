<?php  
	require_once("../layout/initialize.php"); 
			$profiles = Profile::find_all(); 
	require_once("../layout/header.php");	 
?> 
<script type="text/javascript" src="../../js-crud/crud_user.js"></script> 
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start--> 
  <section id="main-content"> 
    <section class="wrapper site-min-height"> 
      <h4>User Module</h4>    		   
      <!-- page start--> 
      <div class="row">      
        <aside class="col-lg-8"> 
          <section> 
            <div class="panel"> 
              <div class="panel-heading"> Add User</div> 
              <div class="panel-body"> 
                <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/insert.php"> 
                <input type="hidden" id="process_type" value="insert"> 
                  <div class="form-group"> 
                    <label  class="col-lg-2">User Name:</label> 
                    <div class="col-lg-8"> 
                      <input type="text" class="form-control" id="user_name" placeholder=" " autocomplete="off"> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-2">First Name:</label> 
                    <div class="col-lg-8"> 
                      <input type="text" class="form-control" id="first_name" placeholder=" " autocomplete="off"> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-2">Last Name:</label> 
                    <div class="col-lg-8"> 
                      <input type="text" class="form-control" id="last_name" placeholder=" " autocomplete="off"> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-2">Email:</label> 
                    <div class="col-lg-8"> 
                      <input type="text" class="form-control" id="email" placeholder=" " autocomplete="off"> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-2">Password:</label> 
                    <div class="col-lg-8"> 
                      <input type="password" class="form-control" id="pwd" placeholder=" " autocomplete="off"> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <div class="col-lg-offset-2 col-lg-10"> 
                      <button type="submit" class="btn btn-info" id="submit">Save</button> 
                      <button type="reset" class="btn btn-default">Cancel</button> 
                      <div id="loading_data"></div> 
                    </div> 
                  </div> 
                </form> 
              </div> 
            </div> 
          </section> 
        </aside> 
        <div class="col-lg-4"> 
          <section class="panel panel-primary"> 
            <header class="panel-heading"> Rules Options: </header> 
            <div class="panel-body"> 
                <form class="form-horizontal tasi-form" role="form" id="form_option"> 
                  <div class="form-group"> 
                    <label  class="col-lg-3 control-label">Profile:</label> 
                    <div class="col-lg-7"> 
                      <select class="form-control" id="profile"> 
                      <option value=""> Select Profile </option> 
                      <?php 
					 	 foreach($profiles as $profile){ 
							 echo "<option value='$profile->id'>$profile->title</option>"; 
						 }?> 
                      </select> 
                    </div> 
                  </div>
                  <div class="form-group"> 
                <label class="col-lg-4">User Type</label>
                <div class="col-lg-8">
                  <label class="checkbox-inline"> 
                    <input type="radio" name="user_type" class="radio" value="admin" checked="checked"> 
                    Admin</label> 
                  <label class="checkbox-inline"> 
                    <input type="radio" name="user_type" class="radio" value="admin_store" > 
                    Admin Store </label> 
                </div> 
              </div> 
                </form> 
              </div> 
          </section> 
        </div> 
      </div> 
      <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
  <?php require_once("../layout/footer.php");?>