<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/SocialComments.php'); 
//check log in  
if($session->is_logged() == false){ 
	redirect_to("../../index.php"); 
} 
if(!empty($_POST["task"]) && $_POST["task"] == "insert"){ 
	$add = new SocialComments(); 
	$add->title = $_POST["title"]; 
	$add->post_id = $_POST["post_id"]; 
	$add->shadow = $_POST["shadow"]; 
	$add->body = $_POST["comment_body"]; 
	$add->inserted_date = date_now(); 
    $add->user_name=$_POST["user_name"]; 
	$add->email=$_POST["email"]; 
	$insert = $add->insert(); 
 	header('Content-Type: application/json'); 
	if($insert){ 
	   $data  = array("status"=>"work"); 
		echo json_encode($data); 
	  
	  }else{ 
		  $data  = array("status"=>"error"); 
		  echo json_encode($data); 
	  }	 
} 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>