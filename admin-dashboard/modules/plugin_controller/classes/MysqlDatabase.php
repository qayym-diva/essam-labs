<?php 
require_once ('ConnectionConfig.php'); 
class MysqlDatabase { 
   public $connection; 
   public $select_db; 
   private $sql_command; 
    
   //call open connection function when instantiation  class 
   public function __construct() { 
       $this->open_connection(); 
   } 
    
   //open connection and select database 
   private function open_connection(){ 
       $this->connection = mysql_connect(DB_SERVER, DB_USER, DB_PASS); 
       if(!$this->connection){ 
           die("Connection Failed".  mysql_error()); 
       }else{ 
           $this->select_db = mysql_select_db(DB_NAME,  $this->connection); 
		   mysql_query("set character_set_server='utf8'");  
		   mysql_query("set names 'utf8'"); 
           if(!$this->select_db){ 
               die("Database Error".  mysql_error()); 
           } 
       } 
   } 
    
   //close connection 
   public function close_connection(){ 
       if(isset($this->connection)){ 
           mysql_close($this->connection); 
           unset($this->connection); 
       } 
   } 
    
   //excute sql statement query 
   public function query($sql){ 
	   $this->sql_command = $sql; 
       $result_set = mysql_query($sql); 
       $this->check_query($result_set); 
       return $result_set; 
   } 
    
   //check if there is sql error 
   private function check_query($query){ 
       if(!$query){ 
		   $error = 'Sql Command Error: '.$this->sql_command."<br />"; 
		   $error .= 'sql Statement error: '.  mysql_error(); 
           die($error); 
       } 
   } 
    
   //fetch array sql query 
   public function fetch_array($result_set){ 
       return mysql_fetch_array($result_set); 
   } 
    
   //num rows 
   public function mysql_num_rows($result_set){ 
       return mysql_num_rows($result_set); 
   }  
    
   //get inserted id 
   public function inserted_id(){ 
       return mysql_insert_id($this->connection); 
   }  
    
   //reutrn true if any process excute in db 
   public function mysql_affected_rows(){ 
       return mysql_affected_rows($this->connection); 
   } 
    
   //prevent xss & sql injection 
   public function escape_values($val){ 
	   $trim_val = trim($val); 
	   $xss_val = html_entity_decode($val); 
       return mysql_real_escape_string($xss_val); 
   } 
} 
$database = new MysqlDatabase(); 
?> 
