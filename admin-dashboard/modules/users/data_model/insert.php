<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/Users.php'); 
//check log in  
if($session->is_logged() == false){ 
	redirect_to("../../index.php"); 
} 
if(!empty($_POST["task"]) && $_POST["task"] == "insert"){ 
	$username = $_POST["user_name"]; 
	$email = $_POST["email"]; 
	header('Content-Type: application/json'); 
	//check rqquierd fields 
    $required_fields = array('user_name'=>"- Insert user name",'first_name'=>'- Insert first name' , 'last_name'=>' - Insert last name' , 'email'=>'- Insert email', 'profile'=>'- Insert profile', 'password'=>'- Insert password'); 
	$check_required_fields = check_required_fields($required_fields); 
	if(count($check_required_fields) == 0){ 
	//check user name doesn`t exist 
		$check_username = Users::find_by_custom_filed('user_name', $username); 
		if(!$check_username){ 
			$check_email = Users::find_by_custom_filed('email', $email); 
			if(!$check_email){ 
				//check email 
				$add = new Users(); 
				$add->user_name = $username;		 
				$add->first_name = $_POST["first_name"]; 
				$add->last_name = $_POST["last_name"]; 
				$add->email = $email; 
				$add->password = sha1(md5($_POST["password"])); 
				$add->inserted_date = date_now(); 
				$add->inserted_by = $session->user_id; 
				$add->user_profile = $_POST["profile"]; 
				$add->user_type = $_POST["user_type"];
				$insert = $add->insert();	 
				if($insert){ 
					$data  = array("status"=>"work"); 
					echo json_encode($data); 
				}else{ 
					$data  = array("status"=>"error"); 
					echo json_encode($data); 
				}				 
			}else{ 
				$data = array("status"=>"email_exist"); 
				echo json_encode($data); 
			} 
		}else{ 
			$data = array("status"=>"username_exist"); 
			echo json_encode($data); 
		} 
  }else{ 
		  //validation error 
		  $comma_separated = implode("<br>", $check_required_fields); 
		  $data  = array("status"=>"valid_error", "fileds"=>$comma_separated); 
		  echo json_encode($data); 
	  }		 
 	 
} 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>