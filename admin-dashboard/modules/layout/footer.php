<footer class="site-footer"> 
  <div class="text-center"> 
      2014 &copy; DivaLab by DIVA. 
      <a href="#" class="go-top"> 
          <i class="icon-angle-up"></i> 
      </a> 
  </div> 
</footer> 
<!--footer end--> 
</section>  
<!-- js placed at the end of the document so the pages load faster --> 
<!--- outo suggest post--> 
<script src="../../js/bootstrap.min.js"></script> 
<script class="include" type="text/javascript" src="../../js/jquery.dcjqaccordion.2.7.js"></script> 
<script src="../../js/jquery.scrollTo.min.js"></script> 
<script src="../../js/jquery.nicescroll.js" type="text/javascript"></script> 
<script type="text/javascript" src="../../assets/data-tables/jquery.dataTables.js"></script> 
<script type="text/javascript" src="../../assets/data-tables/DT_bootstrap.js"></script> 
<script src="../../js/respond.min.js" ></script> 
<script type="text/javascript" src="../../js/jquery.validate.min.js"></script> 
<script src="../../js/gritter.js" type="text/javascript"></script> 
<!--this page plugins--> 
<script src="../../assets/dropzone/dropzone.js"></script> 
<script type="text/javascript" src="../../assets/fuelux/js/spinner.min.js"></script> 
<script type="text/javascript" src="../../assets/bootstrap-fileupload/bootstrap-fileupload.js"></script> 
<script type="text/javascript" src="../../assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> 
<script type="text/javascript" src="../../assets/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script> 
<script type="text/javascript" src="../../assets/bootstrap-daterangepicker/moment.min.js"></script> 
<script type="text/javascript" src="../../assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
<script type="text/javascript" src="../../assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script> 
<script type="text/javascript" src="../../assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script> 
<script type="text/javascript" src="../../assets/jquery-multi-select/js/jquery.multi-select.js"></script> 
<script type="text/javascript" src="../../assets/gritter/js/jquery.gritter.js"></script> 
<script type="text/javascript" src="../../js-crud/insert_invoice.js"></script>
 <script type="text/javascript" src="../../js/tableExport.js"></script>
<script type="text/javascript" src="../../js/jquery.base64.js"></script>
<script type="text/javascript" src="../../js/jspdf/libs/sprintf.js"></script>
<script type="text/javascript" src="../../js/jspdf/jspdf.js"></script>
<script type="text/javascript" src="../../js/jspdf/libs/base64.js"></script>

<!--common script for all pages--> 
<script src="../../js/common-scripts.js"></script> 
<script src="../../js/advanced-form-components.js"></script> 
<!--script for this page only--> 
<script src="../../js/editable-table.js"></script> 
<script src="../../js/jquery.fastLiveFilter.js"></script> 
<script>
    $(function() {
        $('#search_input').fastLiveFilter('#myTable');
    });
</script>
<!-- END JAVASCRIPTS --> 
<script> 
   Dropzone.options.myAwesomeDropzone = {  
 
    }; 
  jQuery(document).ready(function() { 
      EditableTable.init(); 
  }); 
</script> 
<!-- show  category or tags in fancy box--> 
<!-- for datatime picker--> 
<script type="text/javascript" src="../../js/jquery.datetimepicker.js"></script> 
<script> 
$('#start_time').datetimepicker({ 
format:'Y-m-d H:i' 
}); 
$('#end_time').datetimepicker({ 
format:'Y-m-d H:i' 
}); 
$('#start_date').datetimepicker({ 
format:'Y-m-d H:i' 
}); 
$('#end_date').datetimepicker({ 
format:'Y-m-d H:i'	 
}); 
$('#end').datetimepicker({ 
format:'Y-m-d', 
timepicker:false 
}); 
$('#start').datetimepicker({ 
format:'Y-m-d', 
timepicker:false 
}); 
$('.store_time').datetimepicker({
	datepicker:false,
	format:'H:i'
});
$('#birthday').datetimepicker({ 
	timepicker:false, 
	format:'Y-m-d', 
	formatDate:'Y-m-d', 
	 
}); 
</script> 
<script src="../../js/jquery.fastLiveFilter.js"></script> 
<script>
    $(function() {
        $('#search_input').fastLiveFilter('#search_list');
    });
</script>
<!-- js placed at the end of the document so the pages load faster --> 
<script> 
//load tabs by # 
$(document).ready(function(){ 
	var hash = location.hash; 
	if (hash && hash != '#') { 
		hash=hash.substr(1); /* remove the "#" */ 
		$('.nav-tabs a').filter(function () { 
			return $(this).attr('href').indexOf(hash) > -1; 
		}).click();/* click the matching tab */ 
	} 
}); 
</script>
<script>
$(document).on("click", '.DeleteCover',function(){
	$("#imageShow").hide()
	$("#imageVal").val('');
});
$(document).on("click", '.DeleteSlider',function(){
	$("#imageSlider").hide()
	$("#imageVal_slider").val('');
});

</script>	 
<!--<script src="js/jquery.js"></script>--> 
</body> 
</html> 
<?php 
//close connection 
if(isset($database)){ 
$database->close_connection(); 
} 
?>