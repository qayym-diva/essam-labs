<script type="text/javascript" src="../../js/fancybox/jquery.fancybox-1.3.4.pack.js"></script> 
<script type="text/javascript" src="../../js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script> 
<script type="text/javascript"> 
$(document).ready(function (){ 
	$(".category").fancybox({ 
		'width'				: '75%', 
		'height'			: '75%', 
		'autoScale'			: true, 
		'transitionIn'		: 'none', 
		'transitionOut'		: 'none', 
		'type'				: 'iframe' 
	}); 
	$(".address_update_insert").fancybox({ 
		'width'				: '40%', 
		'height'			: '90%', 
		'autoScale'			: false, 
		'transitionIn'		: 'none', 
		'transitionOut'		: 'none', 
		'type'				: 'iframe' 
	});	 
	$(".address_store_update_insert").fancybox({ 
		'width'				: '70%', 
		'height'			: '90%', 
		'autoScale'			: false, 
		'transitionIn'		: 'none', 
		'transitionOut'		: 'none', 
		'type'				: 'iframe' 
	});	 
	$(".tag").fancybox({ 
		'width'				: '75%', 
		'height'			: '75%', 
		'autoScale'			: true, 
		'transitionIn'		: 'none', 
		'transitionOut'		: 'none', 
		'type'				: 'iframe' 
	}); 
	//for resposive filemanager 
	$('.iframe-btn').fancybox({	 
		'width'		: 900, 
		'height'	: 600, 
		'type'		: 'iframe', 
		'autoScale'    	: false 
	}); 
				 
}); 
</script> 
<!-- show user inserted data --> 
<script type="text/javascript"> 
$(document).ready(function (){ 
$("#show_inserted_data").fancybox({ 
			'width'				: '80%', 
			'height'			: '80%', 
			'autoScale'			: true, 
			'transitionIn'		: 'none', 
			'transitionOut'		: 'none', 
			'type'				: 'iframe' 
		}); 
		 
		}); 
		</script> 
		<!-- custmize plugin --> 
<script type="text/javascript"> 
$(document).ready(function (){ 
$("#custimize_plugin").fancybox({ 
			'width'				: '60%', 
			'height'			: '60%', 
			'autoScale'			: 'false', 
			'transitionIn'		: 'none', 
			'transitionOut'		: 'none', 
			'type'				: 'iframe' 
		}); 
		 
		}); 
		</script> 
<!-- show user profile_info --> 
<script type="text/javascript"> 
$(document).ready(function (){ 
$("#profile_info").fancybox({ 
			'width'				: '80%', 
			'height'			: '80%', 
			'autoScale'			: false, 
			'transitionIn'		: 'none', 
			'transitionOut'		: 'none', 
			'type'				: 'iframe' 
		}); 
		 
		}); 
		</script> 
		<script> 
<!-- for filtrition--> 
$(document).ready(function() { 
$("#filter").fancybox({ 
	'transitionIn'  : 'none', 
	'transitionOut' : 'none', 
}); 
}); 
</script> <script type="text/javascript"> 
$(document).ready(function (){ 
$("#image_cover").fancybox({ 
			'width'				: '80%', 
			'height'			: '80%', 
			'autoScale'			: true, 
			'transitionIn'		: 'none', 
			'transitionOut'		: 'none', 
			'type'				: 'iframe' 
		}); 
		 
		}); 
		</script> 
		</script> <script type="text/javascript"> 
$(document).ready(function (){ 
$("#slider_cover").fancybox({ 
			'width'				: '80%', 
			'height'			: '80%', 
			'autoScale'			: true, 
			'transitionIn'		: 'none', 
			'transitionOut'		: 'none', 
			'type'				: 'iframe' 
		}); 
		 
		}); 
		</script>