<?php require_once("../layout/initialize.php");?> 
<?php require_once("../layout/header.php");?> 
      <!--header end--> 
      <!--sidebar start--> 
      <?php require_once("../layout/navigation.php");?> 
      <!--sidebar end--> 
      <!--main content start--> 
      <section id="main-content"> 
          <section class="wrapper site-min-height"> 
              <!-- page start--> 
              <h4>Language Switch Bar page sample.</h4> 
              <p>Please see the right top bar where have language switch bar. We just put a sample language flags with name. You can ad more languages.</p> 
              <!-- page end--> 
          </section> 
      </section> 
      <!--main content end--> 
      <!--footer start--> 
<?php require_once("../layout/footer.php");?>