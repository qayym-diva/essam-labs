<?php 
 	require_once("../layout/initialize.php"); 
 	require_once("../layout/header.php"); 
 ?> 
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start-->  
  <script src="../../js-crud/upload_theme.js"></script> 
  <section id="main-content"> 
    <section class="wrapper site-min-height"> 
      <h4>Theme & Layout Module</h4> 
      <div class="row"> 
        <aside class="profile-info col-lg-9"> 
          <section> 
            <div class="panel "> 
              <div class="panel-heading"> Upload New Theme</div> 
              <div class="panel-body"> 
                <form class="form-horizontal tasi-form" role="form" id="form_theme_upload" enctype="multipart/form-data"> 
                <input type="hidden" name="MAX_FILE_SIZE" value="31457280" />  
                  <div class="form-group"> 
                    <label  class="col-lg-2">Source File:</label> 
                    <div class="col-lg-8"> 
                      <input type="file" name="file" id="upload_file"> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <div class="col-lg-offset-2 col-lg-10"> 
                      <button type="submit" class="btn btn-info" id="submit">Upload</button> 
                      <button type="reset" class="btn btn-default">Cancel</button> 
                      <div id="loading_data"></div> 
                    </div> 
                  </div> 
                </form> 
              </div> 
            </div> 
          </section> 
        </aside> 
      </div> 
      <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
   
  <?php require_once("../layout/footer.php");?> 
