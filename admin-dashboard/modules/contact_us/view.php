<?php  
	require_once("../layout/initialize.php"); 
	$records=ContactUs::find_all("inserted_date","DESC"); 
	require_once("../layout/header.php"); 
?> <!--header end--> 
<script src="../../js-crud/contact_us.js"></script>
      <!--sidebar start--> 
       <?php require_once("../layout/navigation.php");?> 
      <!--sidebar end--> 
      <!--main content start--> 
      <section id="main-content"> 
     <section class="wrapper site-min-height"> 
      <h4>Contact us  Module</h4> 
              <section class="panel"> 
                  <header class="panel-heading"> 
                      View Contact us 
                  </header> 
                  <div class="panel-body"> 
                      <div class="adv-table editable-table "> 
                         
                          <div class="space15"></div> 
                          <table class="table table-striped table-hover table-bordered" id="editable-sample"> 
                          <thead> 
                            <tr> 
                               <th>#</th> 
                              <th><i class=""></i> User Name</th> 
                              <th><i class=""></i> Email</th>
                              <th><i class=""></i> Message</th>
                              <th><i class=""></i> Created  Date</th> 
                              <th>Action</th> 
                            </tr> 
                          </thead> 
                          <tbody id="myTable"> 
                              <?php  
							      $serialize = 1;  
							     foreach($records as $record){ 
								 echo "<tr id='row_{$record->id}'> 
								  <td>{$serialize}</td> 
                                  <td>{$record->user_name}</td> 
								  <td>{$record->email}</td> 
								  <td>{$record->body}</td> 
                                  <td>{$record->inserted_date}</td> 
								   <td>"; 
								   $opened_module_page_fullinfo = 'contact_us/full_info'; 
									//full info
								if(!in_array($opened_module_page_fullinfo, $user_allowed_page_array)){ 
								echo " <a href='' class='btn btn-primary btn-xs tooltips' data-placement='top' data-toggle='tooltip'  
										  data-original-title='Full info' disabled>&nbsp;<i class='icon-info'></i>&nbsp;</a>";	 
							   }else{ 
								 
									echo " <a href='full_info.php?id={$record->id}' class='btn btn-primary btn-xs tooltips' data-placement='top' data-toggle='tooltip'  
										  data-original-title='Full info' >&nbsp;<i class='icon-info'></i>&nbsp;</a>"; 
							   }
								   $opened_module_delete = 'contact_us/delete'; 
									if(!in_array($opened_module_delete, $user_allowed_page_array)){ 
										echo " <a href= '' class='btn btn-primary btn-xs tooltips' data-placement='top' data-toggle='tooltip'  
										data-original-title='Delete' disabled ><i class='icon-remove'></i></a>"; 
									}else{ 
										if($user_profile->global_delete == 'all_records' ){ 
											echo" <a href='#my{$record->id}' data-toggle='modal' class='btn btn-primary btn-xs tooltips'  
											data-placement='top' data-original-title='Delete'  >  <i class='icon-remove'></i></a>"; 
										}else{ 
											echo" <a href= '#myModal'  class='btn btn-primary btn-xs tooltips'  
											data-placement='top' data-toggle='tooltip' data-original-title='Delete'  disabled >  <i class='icon-remove'></i></a>"; 
										} 
									} 
									 	echo "</td>		 
				  </tr> 
				  <div class='modal fade' id='my{$record->id}' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'> 
                                  <div class='modal-dialog'> 
                                      <div class='modal-content'> 
                                          <div class='modal-header'> 
                                              <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button> 
                                              <h4 class='modal-title'>Delete</h4> 
                                          </div> 
                                          <div class='modal-body'> 
                                           <p> Are you sure you want delete  $record->user_name ?</p> 
                                          </div> 
                                         <div class='modal-footer'>
										    <input type='hidden' id='task_type' value='delete'> 
                                              <button class='btn btn-warning confirm_delete' id='{$record->id}' type='button'  data-dismiss='modal' /> Confirm</button> 
											   <button data-dismiss='modal' class='btn btn-default' type='button'>cancel</button> 
                                          </div> 
                                      </div> 
                                  </div> 
                              </div>"; 
			  	 		$serialize++; 
 		            }?> 
              </tbody> 
            </table> 
                      </div> 
                  </div> 
              </section> 
              <!-- page end--> 
          </section> 
      </section> 
      <!--main content end--> 
      <!--footer start--> 
     <?php require_once("../layout/footer.php");?> 
