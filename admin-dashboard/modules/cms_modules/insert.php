<?php  
	require_once("../layout/initialize.php"); 
	$modules = CMSModules::find_all_by_custom_filed('sid', 0, 'sorting', 'ASC'); 
	require_once("../layout/header.php"); 
?> 
<script type="text/javascript" src="../../js-crud/crud_module.js"></script> 
<!--header end--> 
<!--sidebar start--> 
<?php require_once("../layout/navigation.php"); ?> 
<!--sidebar end-->  
<!--main content start--> 
<section id="main-content"> 
  <section class="wrapper site-min-height">  
      <h4>Modules Builder</h4>   
    <!-- page start--> 
    <div class="row"> 
      <aside class="col-lg-8"> 
        <section> 
          <div class="panel"> 
            <div class="panel-heading">New Module Or Page</div> 
            <div class="panel-body"> 
              <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/insert.php"> 
                <input type="hidden" id="process_type" value="insert"> 
                <div class="form-group"> 
                  <label  class="col-lg-2">Title:</label> 
                  <div class="col-lg-8"> 
                    <input type="text" class="form-control" id="title" placeholder=" " autocomplete="off"/> 
                  </div> 
                </div> 
                <div class="form-group"> 
                  <label  class="col-lg-2">Sorting:</label> 
                  <div class="col-lg-8"> 
                    <input type="text" class="form-control" id="sorting" placeholder=" " autocomplete="off" style="width:50px;"/> 
                  </div> 
                </div> 
                <div class="form-group"> 
                  <label  class="col-lg-2">Type:</label> 
                  <div class="col-lg-8"> 
                    <div class="radio"> 
                      <label><input type="radio"  name="structure_type" value="module" checked />module</label> 
                    </div> 
                    <div class="radio" style="padding-right:20px"> 
                      <label><input type="radio" name="structure_type" value="page" />page</label> 
                    </div> 
                  </div> 
                </div> 
                <div class="form-group"  id="icon_available"> 
                  <label  class="col-lg-2">Icon Name:</label> 
                  <div class="col-lg-8"> 
                    <input type="text" class="form-control" id="icon" placeholder=" " autocomplete="off" /> 
                  </div> 
                </div> 
                <div class="form-group" style="display:none" id="shadow"> 
                  <label  class="col-lg-2">Shadow:</label> 
                  <div class="col-lg-8"> 
                    <div class="radio"> 
                      <label> 
                        <input type="radio" name="shadow_status" value="yes" />Yes</label> 
                    </div> 
                    <div class="radio" style="padding-right:20px"> 
                      <label> 
                        <input  type="radio" name="shadow_status" value="no" checked/>No</label> 
                    </div> 
                  </div> 
                </div> 
                <div class="form-group" style="display:none" id="modules"> 
                  <label  class="col-lg-2">Modules</label> 
                  <div class="col-lg-8"> 
                    <select  class="form-control" id="module_list"> 
                    	<option value="">Select Module</option> 
                      <?php foreach ($modules as $module) : ?> 
                      <option value="<?php echo $module->id;?>"> <?php echo $module->title;?></option> 
                      <?php endforeach;?> 
                    </select> 
                  </div> 
                </div> 
                <div class="form-group"> 
                  <label  class="col-lg-2">Source Destination:</label> 
                  <div class="col-lg-8"> 
                    <input type="text" class="form-control" id="file_source" placeholder="" autocomplete="off"/> 
                  </div> 
                </div> 
                <div class="form-group"> 
                  <div class="col-lg-offset-2 col-lg-10"> 
                    <button type="submit" class="btn btn-info" id="submit">Save</button> 
                    <button type="reset" class="btn btn-default">Cancel</button> 
                    <div id="loading_data"></div> 
                  </div> 
                </div> 
              </form> 
            </div> 
          </div> 
        </section> 
      </aside> 
      <div class="col-lg-4"> 
        <section class="panel panel-primary"> 
          <header class="panel-heading"> Modules & Sub Pages </header> 
          <div class="panel-body" style="max-height: 413px;overflow-y: auto;"> 
            <ul class="nav prod-cat" id="module_page_view_list"> 
              <?php  
			  	$module_serial = 1;	 
				foreach ($modules as $module): ?> 
              <li> <a href="update.php?id=<?php echo $module->id?>" class="active"><i class=" icon-angle-right "></i>  
			  <?php echo $module_serial."-".$module->title;?></a> 
                <ul class="nav"> 
                  <?php  
						$pages = CMSModules::find_all_by_custom_filed('sid', $module->id, 'sorting', 'ASC'); 
						 $page_serial = 1; 
						foreach ($pages as $page): 
					?> 
                  <li><a href="update.php?id=<?php echo $page->id?>"><?php echo $page_serial."-".$page->title;?></a></li> 
                  <?php  
				  $page_serial++; 
				  endforeach;?> 
                </ul> 
              </li> 
              <?php  
			  $module_serial++; 
			  endforeach;?> 
            </ul> 
          </div> 
        </section> 
      </div> 
    </div> 
    <!-- page end-->  
  </section> 
</section> 
<!--main content end-->  
<!--footer start--> 
<?php require_once("../layout/footer.php"); ?>