<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/CMSModules.php'); 
require_once('../../../../classes/Profile.php'); 
require_once('../../../../classes/ProfileModulesAccess.php'); 
require_once('../../../../classes/ProfilePagesAccess.php'); 
require_once('../../../../classes/Users.php'); 
//check  session user  log in 
if($session->is_logged() == false){ 
	redirect_to("../../../index.php"); 
} 
// get user profile   
$user_data = Users::find_by_id($session->user_id); 
// get user profile data 
$user_profile  = Profile::Find_by_id($user_data->user_profile); 
// check if the user profile block 
if($user_profile->profile_block == "yes"){ 
   redirect_to("../../../index.php");	 
} 
//send json data 
 header('Content-Type: application/json'); 
if(!empty($_POST["task"]) && $_POST["task"] == "insert"){ 
	$required_fields = array('title'=>"- Insert title", 'sorting'=>'- Insert sorting' ,'source'=>'- Insert source Destination'); 
	$check_required_fields = check_required_fields($required_fields); 
	if(count($check_required_fields) == 0){ 
		$structure_type = $_POST["structure_type"]; 
		$module_sid = $_POST["module"]; 
		$add = new CMSModules(); 
		$add->title = $_POST["title"]; 
		$add->sid = $_POST["module"]; 
		$add->sorting = $_POST["sorting"]; 
		$add->shadow = $_POST["shadow"]; 
		$add->icon = $_POST["icon"]; 
		$add->file_source = $_POST["source"]; 
		$add->type = $structure_type; 
		$insert = $add->insert();	 
		if($insert){ 
			$inserted_id = $database->inserted_id(); 
			//if type module insert in into profile module access 
			//get all profiles id 
			$profiles = Profile::find_all(); 
			foreach($profiles as $profile){ 
				if($structure_type == "module"){ 
					//insert into ProfileModulesAccess 
					$add_module = new ProfileModulesAccess(); 
					$add_module->profile_id = $profile->id; 
					$add_module->module_id = $inserted_id; 
					$add_module->access =  'no'; 
					$add_module->inserted_by = $session->user_id; 
					$add_module->inserted_date = date_now(); 
					$insert_module = $add_module->insert(); 
					unset($add_module); 
				}else{ 
					//insert into ProfilePagesAccess 
					$add_page = new ProfilePagesAccess(); 
					$add_page->profile_id = $profile->id; 
					$add_page->module_id = $module_sid; 
					$add_page->Page_id = $inserted_id; 
					$add_page->access =  'no'; 
					$add_page->inserted_by = $session->user_id; 
					$add_page->inserted_date = date_now(); 
					$insert_page= $add_page->insert(); 
					unset($insert_page);				 
					 
				} 
			} 
			//insert page 
			$data  = array("status"=>"work", "id"=>"$inserted_id", "title"=>"$add->title"); 
			echo json_encode($data); 
		}else{ 
			$data  = array("status"=>"error"); 
			echo json_encode($data); 
		} 
	}else{ 
		  //validation error 
		  $comma_separated = implode("<br>", $check_required_fields); 
		  $data  = array("status"=>"valid_error", "fileds"=>$comma_separated); 
		  echo json_encode($data); 
	  }			 
} 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>