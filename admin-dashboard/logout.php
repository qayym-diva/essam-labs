<?php
	require_once("../classes/Session.php");
	require_once("../classes/Functions.php");
	$session->logout();
	redirect_to("index.php");
?>