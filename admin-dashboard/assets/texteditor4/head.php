<!-- Place inside the <head> of your HTML -->
<script type="text/javascript" src="../../assets/texteditor4/tinymce/tinymce/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
selector: "textarea",
theme: "modern",
width: 550,
height: 220,
subfolder:"",
document_base_url: '',
relative_urls: false,
convert_urls: false,
remove_script_host : false,
plugins: [
"advlist autolink link image lists charmap print preview hr anchor pagebreak",
"searchreplace wordcount visualblocks visualchars code insertdatetime media nonbreaking",
"table contextmenu directionality emoticons paste textcolor filemanager template  save autoresize lineheight"
],
//fullscreen
templates: [ 
		
	
		{title: 'Doctors and patients English template', description: 'Doctors and patients  template', url: '../../../texteditor-template/posts_after_slider.html'} , 
		{title: 'Lab And Latest News Section', description: 'Doctors and patients  template', url: '../../../texteditor-template/lab_and_latest_news.html'} , 
		{title: 'About Us Template ', description: 'About Us template', url: '../../../texteditor-template/About Us.html'} , 
		{title: 'Services Template ', description: 'Services body template', url: '../../../texteditor-template/service_body_details.html'} 

		
		
		
		
		
		
	
		
		
    ],
autoresize_min_height:220,
autoresize_max_height:220,
width:550,
'object_resizing' : false,
image_advtab: true,
directionality : 'ltr',
toolbar: " undo redo |styleselect | fontselect | fontsizeselect  bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect forecolor backcolor | link unlink anchor | image media | removeformat | print code template preview save fullscreen lineheight ",
fontsize_formats: "8pt 9pt 10pt 11pt 12pt 26pt 36pt",
}); 
</script>