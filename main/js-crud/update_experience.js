jQuery(document).ready(function($) {

	// process the form

	$('#experience_form_update').submit(function() {

		 $('#update_experience').attr('disabled', 'disabled'); 

		var formData = {

			lang: $('#lang').val(), 

			customer_id : $('#customer_id').val(),  

			experience_id:$('#experience_id').val() ,

			job_title : $('#job_title').val(),  

			job_role : $('#job_role').val(),  

			career_level : $('#career_level').val(), 

			ex_type : $('#experience_type').val() , 

			company_name : $('#company_name').val() , 

			from_date : $('#datetimepicker4').val() , 

			to_date : $('#datetimepicker5').val() 

			


		};

		// process the form

		$.ajax({

			type 		: 'POST', // define the type of HTTP verb we want to use (POST for our form)

			url 		: 'main/data_model/update_experience.php', // the url where we want to POST

			data 		: formData, // our data object

			success : function(data) {
				 
				  
				if(data.status == 'work'){

					$("#updating_current_experience").html("<div class='alert alert-block alert-success'>"+data.message+"</div >");
					window.location.href="user-profile.php?lang="+$('#lang').val();
				 
					//$('#general_info')[0].reset();

				}else if (data.status == 'not_work'){
					$("#updating_current_experience").html("<div class='alert alert-block alert-danger'>"+data.message+"</div >");
					 
				}else{

					$("#updating_current_experience").html("<div class='alert alert-block alert-danger'>"+data.message+"</div >");

				}
			 

 		 
			

				

			}

		});

	return false;

	});

	

	

		

});