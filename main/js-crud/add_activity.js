jQuery(document).ready(function($) {

	// process the form

	$('#activity_form').submit(function() {

		 $('#add_new_activity').attr('disabled', 'disabled'); 

		var formData = {

			lang: $('#lang').val(), 

			customer_id : $('#customer_id').val(),  

			activity_name:$('#activity_name').val(), 

			activity_type:$('#activity_type').val(), 

			from_date : $('#datetimepicker2').val() , 
			
			to_date:$('#datetimepicker3').val() , 

			role:$('#role').val() , 

			description:$('#description').val()


		};

		// process the form

		$.ajax({

			type 		: 'POST', // define the type of HTTP verb we want to use (POST for our form)

			url 		: 'main/data_model/add_activity.php', // the url where we want to POST

			data 		: formData, // our data object

			success : function(data) {
				 
				 
				if(data.status == 'work'){

					$("#adding_new_activity").html("<div class='alert alert-block alert-success'>"+data.message+"</div >");
						window.setTimeout(function(){location.reload();},220);
					//$('#general_info')[0].reset();

				}else if (data.status == 'not_work'){
					$("#adding_new_activity").html("<div class='alert alert-block alert-danger'>"+data.message+"</div >");
					$('#add_new_activity').removeAttr('disabled'); 
				}else if(data.status == 'email_exists'){
					$("#adding_new_activity").html("<div class='alert alert-block alert-danger'>"+data.message+"</div >");
					$('#add_new_activity').removeAttr('disabled'); 
				}else if(data.status == 'pass_length'){
					$("#adding_new_activity").html("<div class='alert alert-block alert-danger'>"+data.message+"</div >");
					$('#add_new_activity').removeAttr('disabled');
				}else{
					$("#adding_new_activity").html("<div class='alert alert-block alert-danger'>"+data.message+"</div >");
					}

				

			}

		});

	return false;

	});

	

	

		

});