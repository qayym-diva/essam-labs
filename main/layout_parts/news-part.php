<section class="col-md-12 our-news clearfix"><!--start of our-news-->
      <div class="col-md-8 clearfix">
      <div class="row">
       <?php
       $lab_and_latest_news  = $define_node->get_node_content(9 , $lang_info->id) ;

       echo $lab_and_latest_news->summary ;
       ?>
      </div>
        <div class="row"><!--row-->
        <?php
          $news_info = $define_node->get_node_content(10 , $lang_info->id) ;
          $image = $define_node->get_image_for_node(10 ) ;


        ?>
          <div class="col-md-11 doct-adv" style="background-image: url('media-library/<?php echo $image->cover_image ;  ?>'); ">
            <div class="col-md-12 wrapping2"><!--wrapping2-->
              <?php $summary_stripped = strip_tags($news_info->summary) ;  ?>
              <?php $body_stripped = strip_tags($news_info->body) ;  ?>
              <h2 class="c-red"><?php echo $summary_stripped ;  ?></h2>
              <p class="c-black"><?php echo $body_stripped ;  ?></p>
            </div><!--end wrapping2-->
          </div><!--doct-adv-->
         </div><!--end row-->
      </div>
