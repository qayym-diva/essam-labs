  <section class="col-md-12 slider"><!--start of slider-->
        <div id="myCarousel" class="carousel slide" data-ride="carousel"><!--mycarousel-->
            <!-- Indicators -->
            <ol class="carousel-indicators">

               <?php  
                 $slider = $define_node->front_node_data(null,null,null,null,$lang_info->id,'yes','yes',null,null,null,null,null,'many',3,null); 
                if($slider){
                $index = 0 ; 
                if($slider){
                foreach($slider as $slide){
                echo " <li class='" ; 
                if($index == 0 ){
                echo " active ";  
                
                }
                echo " ' data-slide-to='$index' data-target='#myCarousel' ></li>"; 
                $index++ ;    
                
                }
                
                } 
                ?>

            </ol>
            <!-- Indicators -->


            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox"><!--carousel-inner-->

               <?php 
                $index = 0 ; 
                foreach($slider as $slide){
                
                    $path = ""; 
                    if($slide->node_type == "page"){
                    $path = "content.php";
                    }else if($slide->node_type == "post"){
                    $path = "post_details.php";
                    }else if($slide->node_type == "event"){
                    $path = "event_details.php";
                    } 
                    echo " <div class ='item "; 
                    if($index == 0 ){
                    echo "active "; 
                    }
                    echo " '>"; 
                 

                    echo "<img src='media-library/slider/$slide->slider_cover' alt='Chania width='460' height='345'>";
                    
                    echo "<article class='carousel-caption'>";
                    echo " <h1 class='c-red caption-head'><a href='$path?alias=$slide->alias&lang=$lang'>$slide->title</a></h1>";
                    $clean_text =   strip_tags($slide->summary ); 
                    $final_text = string_limit_words($clean_text,25); 
                    echo "<p class='c-black'>$final_text</p>"; 
                    echo "<div class='col-md-12 read-more c-red'><!--read-more-->
                          <p class='text-right'><a href='$path?alias=$slide->alias&lang=$lang'>$read_more</a>
                          <span class='fa fa-angle-double-right'></span>
                          </p>
                        </div>" ; 
                     
                    echo "</article></div>";
                       $index++;
                    
                }
                
                }else{
                    echo "a7a";
                }
              ?>

            </div><!--carousel-inner-->
          </div><!--end of my carousel-->
      </section><!--end of slider-->