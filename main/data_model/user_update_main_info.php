<?php
require_once('../../classes/FrontSession.php');
require_once('../../classes/Functions.php');
require_once('../../classes/MysqlDatabase.php');
require_once('../../classes/CustomerInfo.php');
require_once('../../localization/'.$_POST["lang"].'/validation_label.php');
header('Content-Type: application/json');
$update = CustomerInfo::find_by_id($front_session->user_id);
//check email exist
//check phone exist
$email_exist = "no";
$mobile_exist = "no";
if($update->email != $_POST["email"]){
	//check email exist
	$check_email = CustomerInfo::find_all_by_custom_filed('email', $_POST["email"]); 
	if(count($check_email) > 0){ 
		$email_exist = "yes";
	}
}elseif($update->mobile != $_POST["mobile"]){
	//check phone exist
	$check_phone = CustomerInfo::find_all_by_custom_filed('mobile', $_POST["mobile"]); 
	if(count($check_phone) > 0){ 
		$mobile_exist = "yes";
	}
}
//user passwrod

$required_fields = array('first_name'=>$vfname,'last_name'=>$vlname,'email'=>$vemail,'mobile'=>$vmobile,'birthday'=>$vbirthday);  
$check_required_fields = check_required_fields($required_fields);  
if(count($check_required_fields) != 0){  
	$comma_separated = implode("<br>", $check_required_fields); 
	  $data  = array("status"=>"not_work", "message"=>$comma_separated); 
	echo json_encode($data); 
}elseif(!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {  
	$data = array("status"=>"not_work","message"=>$vemail_invalid);		
	echo json_encode($data); 	
}elseif($email_exist == "yes"){
	$data = array("status"=>"not_work","message"=>$vemail_exist);
	echo json_encode($data); 
}elseif($mobile_exist == "yes"){
	$data = array("status"=>"not_work","message"=>$vmobile_exist);
	echo json_encode($data); 
}else{
	$update->first_name = $_POST["first_name"]; 
	$update->last_name = $_POST["last_name"]; 
	$update->email = $_POST["email"]; 
	$update->mobile = $_POST["mobile"]; 
	$update->company = $_POST["company"]; 
	$update->birthday = $_POST["birthday"]; 
	$update->account_status = $update->account_status; 
	$edit = $update->update(); 
	if($edit){
		$data = array("status"=>"work","message"=>$vupdate_complete); 
		echo json_encode($data);
	}else{
		$data  = array("status"=>"not_work","message"=>$verror_from_system);
		echo json_encode($data);
	}	
}
//close connection
if(isset($database)){
	$database->close_connection();
}
?>
