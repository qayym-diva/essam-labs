<?php
require_once('../../classes/FrontSession.php');
require_once('../../classes/Functions.php');
require_once('../../classes/MysqlDatabase.php');
require_once('../../classes/CustomerInfo.php');
require_once('../../classes/ForgetPassword.php');
require_once('../../localization/'.$_POST["lang"].'/validation_label.php');
require_once('../../localization/'.$_POST["lang"].'/form_label.php');

header('Content-Type: application/json');

$new_pwd = md5($_POST["new_password"]);
		
if(strlen($_POST["new_password"]) < 6){
	$data = array("status"=>"password_len","message"=>$vpass_length);
	echo json_encode($data); 			
}else{
	//get user is by code 
	$code =  $_POST["code"];
	$get_info_by_code = ForgetPassword::find_by_custom_filed('code',$code);
	if($get_info_by_code){
		$user_info = CustomerInfo::find_by_id($get_info_by_code->user_id);
		$update = new CustomerInfo();
	    $update->id = $user_info->id; 
		$update->first_name =$user_info->first_name; 
		$update->last_name = $user_info->last_name; 
		$update->email = $user_info->email; 
		$update->password = $new_pwd; 
		$update->mobile = $user_info->mobile; 
		$update->gender =$user_info->gender; 
		$update->city = $user_info->city; 
		$update->area = $user_info->area; 
		$update->street = $user_info->street; 
		$update->colleage_department = $user_info->colleage_department; 	 
		$update->otherMobile = $user_info->otherMobile; 
		$update->nationality = $user_info->nationality;
		$update->registeration_date = $user_info->registeration_date;
		$update->account_status = $user_info->account_status;
		$update->activation_code = $user_info->activation_code;
		$update->birth_date = $user_info->birth_date;
		 

		 
	
		$edit = $update->update(); 
		if($edit){
			$find_code = ForgetPassword::find_by_custom_filed('code',$code);
			$find_code->delete();
			$data = array("status"=>"work","message"=>$vupdate_complete); 
			echo json_encode($data); 	
		}else{
			$data = array("status"=>"not_work","message"=>$verror_from_system);
			echo json_encode($data);
		}
	}
	
}
//close connection
if(isset($database)){
	$database->close_connection();
}
?>