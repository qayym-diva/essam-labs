<?php
require_once("initialize_classes_files.php"); 
require_once("main/layout_parts/header.php");
require_once('main/layout_parts/nav.php');
	if(empty($node_alias)){
		redirect_to('index.php');
	}
	
	//retrive layout 
	$get_event_content = $define_node->front_node_data(null,"event",$node_alias,null,$lang_info->id,null,null,null,null, null,null,null,"one");
	$node_type = 'event';
	if($get_event_content){				
		$event_layout_info = $define_node->front_get_model($get_event_content->id);
		
		//layout
		require_once("main/layout_templates/".$event_layout_info->layout_name.".php");
	}else{
		echo "<h1>Page not found</h1>
			<p>We don't think you came to the right page. Please use the website navigation to reach what you want.</p>";	
	}
	
	require_once("main/layout_parts/footer.php")
?>