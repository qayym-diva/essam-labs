 <section class="col-md-12 give-padding-tb"><!--start of internal content-->
        <div class="container">
         <div class="row clearfix">
  <?php
//get page category
//show posts
$define_page_categories = new NodesSelectedTaxonomies();
$define_page_categories->enable_relation();
$page_categories = $define_page_categories->return_node_taxonomy($get_page_content->id,'page','category','one',$lang_info->id);
$page_categories_array = array();
if(count($page_categories) > 0){
	
  // foreach($page_categories as $category){
	//   $page_categories_array[$category->id] = $category->taxonomy_name;
  // }
  //get categories ids
  // $page_categories_ids = array();
  // foreach($page_categories_array as $key=>$value){
	//   $page_categories_ids [] = $key;
  // }
  
// $count = count($page_categories_ids);
// $count = count($page_categories_ids);
  //  $index = 1;
  // $categories = implode(",",$page_categories_ids);
  //  //count all posts in the page
   
  // get categories titles and list it as tabs for filtration  : 
  $categories_data = $define_taxonomy_class->front_taxonomies_data(null,'category',null,null,'ASC',
	                 'many',$lang_info->id,$page_categories->id,null,null,null); 


        // begin of ul nav as categories  to filtrate gelleries  

        echo "<ul class='nav nav-tabs  internal-gallery-tabs' role='tablist'>" ;        
        $category_active = 0 ; 
        foreach($categories_data as $get_one_category){
            echo " <li role='presentation' class=' " ;
            if($category_active == 0 ){
              echo " active  ";
            }  
            echo " '> " ; 
            echo "<a href='#$get_one_category->id' aria-controls='profile' role='tab' data-toggle='tab'>$get_one_category->name </a></li>";
            $category_active++;  
           
        }

        echo "</ul>"; 




//   $page_all_post = $define_node->front_node_data(null,"post",null,null,$lang_info->id,null,null,null,$categories, null,null,null,"many");
//   //total posts
//   $total_posts = count($page_all_post);
//   //get page id if exist
//   $page_number = !empty($_GET['page']) ? (int)$_GET['page'] : 1;
//  //number of posts in one page
//   $per_page =4 ;
//   $pagination = new Pagination($page_number , $per_page, $total_posts);
//   //calculate the offset
//   $offset = $pagination->offset();
//   $count = $offset+1;
 
  
  ?>
  
    
<?php
 // start of gallery content 
 echo "<div class='tab-content internal-gallery-content'>" ; 
 $gallery_index = 0 ; 
 
  foreach($categories_data as $get_one_category){


//   $page_all_post = $define_node->front_node_data(null,"post",null,null,$lang_info->id,null,null,null,$get_one_category->id, null,null,null,"many");
//   //total posts
//   $total_posts = count($page_all_post);
//   //get page id if exist
//   $page_number = !empty($_GET['page']) ? (int)$_GET['page'] : 1;
//  //number of posts in one page
//   $per_page =4 ;
//   $pagination = new Pagination($page_number , $per_page, $total_posts);
//   //calculate the offset
//   $offset = $pagination->offset();
//   $count = $offset+1;


       	   	echo "<div role='tabpanel' class='row clearfix tab-pane "; 
                if($gallery_index == 0 ){
                  echo " active "; 
                }
            echo " ' "; 
            echo "  id='$get_one_category->id'>";



              // // loop through posts : 
               $first_post = $define_node->front_node_data(null,"post",null,null,$lang_info->id,null,null,null,$get_one_category->id, null,null,null,"many",1,null);
                
                // echo $count_images;
                if($first_post){

                  foreach($first_post as $the_first_post){
                    $find_images_for_post = $define_image_gallery->get_images($the_first_post->id); 
                $count_images = count($find_images_for_post) ; 
                  
                if($the_first_post->cover_image){
                  $image_path = get_image_src($the_first_post->cover_image,"large");
                }else{
                  $image_path = "";
                }
                         
                echo "<div class='col-md-5 internal-gallery-item internal-gallery-left'>
                      <a href='$image_path'  data-fresco-group='$the_first_post->id' class='fresco' style='background-image: url($image_path);'>
                      <div class='col-md-12 give-semi-gradient give-absolute-full '></div>
                      <div class='col-md-12 give-absolute-rlb give-padding-reg main-offers-caption'>
                        <h3 class='col-md-12 give-float-left'>$the_first_post->title</h3>
                        <p class='col-md-12 give-float-right'>$count_images</p>
                      </div>
                      </a>";     
                      echo "<div>"; 
                      foreach($find_images_for_post as $image){
                        echo "<a href='media-library/$image->image'  data-fresco-group='$the_first_post->id' class='fresco'></a>"; 
                      }
                      echo "</div>"; 
                  


              echo "</div>"; 

              }

              }




              $second_post = $define_node->front_node_data(null,"post",null,null,$lang_info->id,null,null,null,$get_one_category->id, null,null,null,"many",2,1);
              //  $find_images_for_post = $define_image_gallery->get_images($first_post->id); 
              //   $count_images = count($find_images_for_post) ;
              if($second_post){


               

                    echo "<div class='col-md-7 internal-gallery-item internal-gallery-right'>";
                   
                  foreach($second_post as $second){

                     if($second->cover_image){
                        $image_path = get_image_src($second->cover_image,"large");
                      }else{
                        $image_path = "";
                      }

                  $find_images_for_post = $define_image_gallery->get_images($second->id); 
                  $count_images = count($find_images_for_post);
                  
                   echo " <div class='col-md-12 internal-gallery-item internal-gallery-half'>";
                       
                   echo " <a href='$image_path'  data-fresco-group='$second->id' class='fresco' style='background-image: url($image_path);'>
                      <div class='col-md-12 give-semi-gradient give-absolute-full '></div>
                      <div class='col-md-12 give-absolute-rlb give-padding-reg main-offers-caption'>
                        <h3 class='col-md-12 give-float-left'>$second->title</h3>
                        <p class='col-md-12 give-float-right'>$count_images</p>
                      </div>
                    </a>"; 

                        echo "<div>";  
                      foreach($find_images_for_post as $image){
                        echo "<a href='media-library/$image->image'  data-fresco-group='$second->id' class='fresco'></a>"; 
                      }
                      echo "</div>"; 

                 
                       echo "</div>"; 

                  }

                    

                  echo "</div>"; 




              }


              $fourth_post = $define_node->front_node_data(null,"post",null,null,$lang_info->id,null,null,null,$get_one_category->id, null,null,null,"many",1,3);
              if($fourth_post){
                foreach($fourth_post as $f_post){


                
                if($f_post->cover_image){
                  $image_path = get_image_src($f_post->cover_image,"large");
                }else{
                  $image_path = "";
                }

                 $find_images_for_post = $define_image_gallery->get_images($f_post->id); 
                $count_images = count($find_images_for_post) ;

                 echo "<div class='col-md-all internal-gallery-item'>";

                 echo " 
                    <a href='$image_path'  data-fresco-group='$f_post->id' class='fresco' style='background-image: url($image_path);'>
                      <div class='col-md-12 give-semi-gradient give-absolute-full '></div>
                      <div class='col-md-12 give-absolute-rlb give-padding-reg main-offers-caption'>
                        <h3 class='col-md-12 give-float-left'>$f_post->title</h3>
                        <p class='col-md-12 give-float-right'>$count_images</p>
                      </div>
                    </a>"; 

                         
                      echo "<div>"; 
                      foreach($find_images_for_post as $image){
                        echo "<a href='media-library/$image->image'  data-fresco-group='$f_post->id' class='fresco'></a>"; 
                      }
                      echo "</div>";


                      echo "</div>";

                  }


              }


            echo "</div>"; // end of tab panel 

          $gallery_index++;      
			 
		  }//end of foreach

      echo "</div>"; // end of tab content 
		  
 }//end of page_categories 
 ?>





 


    </div>
 </section>

