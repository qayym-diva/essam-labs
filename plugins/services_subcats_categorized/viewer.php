  <section class='col-md-12 vision clearfix'><!--end of vision-->

            <div class='col-md-4 service-selector'><!--start of service-selector-->
              <h2 class='text-center red-bg'>Choose from Services</h2>

              <?php
                $define_page_categories = new NodesSelectedTaxonomies();
                $define_page_categories->enable_relation();
                // main services category id
                $main_services_category = $_GET['mid'] ;
                $this_sub_category = $_GET['alias'] ;

                $categories_data = $define_taxonomy_class->front_taxonomies_data(null,'category',null,null,'ASC',
                   'many',$lang_info->id,$main_services_category,null,null,null);
                foreach($categories_data as $sub_category){

                    echo "<div class='dropdown " ;
                    if($sub_category->alias ==  $this_sub_category){
                      echo " open " ;
                    }
                    echo " '>" ;
                    echo "  <button class='btn dropdown-toggle c-black drop-btn' type='button' data-toggle='dropdown' " ;
                      if($sub_category->alias ==  $this_sub_category){
                      echo " aria-expanded='true' ";
                    }
                    echo " >$sub_category->name
                <span class='fa fa-angle-double-down down-caret'></span></button>";
                $get_sub_categories = $define_taxonomy_class->front_taxonomies_data(null,'category',null,null,'ASC',
                   'many',$lang_info->id,$sub_category->id,null,null,null);
                if($get_sub_categories){

                  echo " <ul class='dropdown-menu drop-items'> ";
                    foreach ($get_sub_categories as $cat) {

                         $get_nodes = $define_page_categories->return_taxonomy_nodes($cat->id,'page','category','one',$lang_info->id);
                         if($get_nodes){
                          $this_node_alias = $get_nodes->node_alias;
                         }else{
                          $this_node_alias = '' ;
                         }

                      echo " <li><a href='content.php?lang=$lang&alias=$this_node_alias&mid=$main_services_category'>$cat->name</a></li>" ;
                    }
                  echo "</ul>";
                }

                echo " </div>" ;

                }

              ?>
              </div>


             <?php
              /**
               * now we're gonna fetch every post under sub category
               */

                $define_page_categories = new NodesSelectedTaxonomies();
                $define_page_categories->enable_relation();
                $page_categories = $define_page_categories->return_node_taxonomy($get_page_content->id,'page','category','one',$lang_info->id);

                $page_all_post = $define_node->front_node_data(null,"post",null,null,$lang_info->id,null,null,null,$page_categories->id, null,null,null,"many");
                //total posts
                $total_posts = count($page_all_post);
                //get page id if exist
                $page_number = !empty($_GET['page']) ? (int)$_GET['page'] : 1;
               //number of posts in one page
                $per_page =6 ;
                $pagination = new Pagination($page_number , $per_page, $total_posts);
                //calculate the offset
                $offset = $pagination->offset();
                $count = $offset+1;

                // get sub_sub_category id from it's alias :
                 $get_sub_categories = $define_page_categories->return_node_taxonomy($get_page_content->id,'page','category','one',$lang_info->id);
                $List_posts_for_blog = $define_node->front_node_data(null,"post",null,null,$lang_info->id,null,null,null,$get_sub_categories->id, null,null,null,"many",$per_page,$offset);



             ?>

              <div class='col-md-8'><!--start of col8-->
                  <?php
                    /**
                     *  start here listing all posts  under this category
                     */

                    if($List_posts_for_blog){
                      echo "<div class='clearfix'>" ;
                      foreach ($List_posts_for_blog as $post) {
                        echo "<div class='col-md-6 pagi-wrapp'>" ;
                        // check if post has image or not
                        if($post->cover_image){
                          echo "<a href='post_details.php?lang=$lang&alias=$post->alias&mid=$main_services_category'> <img src='media-library/$post->cover_image' alt='$post->title'></a>";
                        }else{
                          echo " <img src='#' alt='$post->title'>";
                        }

                        echo "<div class='col-md-12 text-wrapping'>";

                        echo "<h3 class='c-red'><a href='post_details.php?lang=$lang&alias=$post->alias&mid=$main_services_category'>$post->title</a></h3>";
                        $post_summary = strip_tags($post->summary) ;
                        $post_summary_limited = string_limit_words($post_summary , 15);
                        echo "<p class='c-black c-inline pagi-para'>$post_summary_limited<a href='post_details.php?lang=$lang&alias=$post->alias&mid=$main_services_category' class='c-red'> More..</a></p>";
                        echo "</div>";
                        echo "</div>";


                      }
                      echo "</div>";
                    }
                  ?>

         <div class='col-md-12'><!--pagination-->
          <?php
            echo "<nav class='col-md-12 pagination-wrap'>";
        if($pagination->total_pages() > 1){
        echo "<ul class='pagination'>";
        if($pagination->has_previous_page()){
          $previous_page = $pagination->previous_page();

          echo "<li>
              <a href='content.php?lang=$lang&alias=$node_alias&page=$previous_page&mid=$main_services_category' aria-label='Previous'>
              <span class='fa fa-caret-left'></span>

              </a>
            </li>";
        }
        for($i=1; $i <= $pagination->total_pages(); $i++) {
            if($i == $page_number){
            $active = "  active ";
            }else{
            $active = " ";
            }
        echo "<li class='$active'><a href='content.php?lang=$lang&alias=$node_alias&page={$i}&mid=$main_services_category'>$i</a></li>";


        }

        // check next page
        if($pagination->has_next_page()) {
        $next_page = $pagination->next_page();
        echo "<li><a href='content.php?lang=$lang&alias=$node_alias&page=$next_page&mid=$main_services_category' aria-label='Next'>
               <span class='fa fa-caret-right'></span></a></li>";
        }
        echo "</ul>";

        }
        echo "</nav>";
        ?>


          </div><!--end of pagination-->

          </div><!--row-->
        </section><!--end of vision-->
