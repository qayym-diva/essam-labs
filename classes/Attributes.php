<?php
require_once 'CRUD.php';
class Attributes extends CRUD{
   //calss attributes
   public $id;
   public $type;   
  
   //relation table attribute
   //push attributes for relational tables
   public function enable_relation(){
		array_push(static::$primary_fields , '','');
   }   
   //define table name and fields
	protected static $table_name = 'attributes';
	protected static $primary_fields = array('id', 'type');
	
	
	
   
}
?>