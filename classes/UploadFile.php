<?php 
class UploadFile{ 
	public  $file_name; 
	public  $new_file_name; 
	public  $file_size; 
	public  $file_type; 
	public  $file_extension; 
	private $temp_path; 
	private $file_location; 
	public  $succeeded; 
	//hold errors 
	public $errors = array(); 
	//valid file types 
	private $valid_type = array(); 
	//folder path 
	private $upload_dir; 
	//allowed size 
	private $allowed_size; 
	//php errors messages 
	//set path and type when initialize class 
	function __construct($path, $type, $size){  
		$this->upload_dir = $path; 
        $this->valid_type = $type; 
		$this->allowed_size = $size; 
    } 
	//error types 
	private $upload_errors = array( 
		  // http://www.php.net/manual/en/features.file-upload.errors.php 
		  UPLOAD_ERR_OK 		=> "No errors.", 
		  UPLOAD_ERR_INI_SIZE  	=> "Larger than upload_max_filesize.", 
		  UPLOAD_ERR_FORM_SIZE 	=> "Larger than form MAX_FILE_SIZE.", 
		  UPLOAD_ERR_PARTIAL 	=> "Partial upload.", 
		  UPLOAD_ERR_NO_FILE 	=> "No File Was Uploaded.", 
		  UPLOAD_ERR_NO_TMP_DIR => "No temporary directory.", 
		  UPLOAD_ERR_CANT_WRITE => "Can't write to disk.", 
		  UPLOAD_ERR_EXTENSION 	=> "File upload stopped by extension." 
	); 
	//get file information 
	public function attach_file($file){ 
		//replace any white spaces with _ 
		$file = preg_replace('/\s+/', '_',$file); 
		if(!$file || empty($file) || !is_array($file)){ 
			$this->errors[] = "No File Was Uploaded"; 
			$this->succeeded = "no"; 
			return false; 
		}elseif($file['error'] != 0){ 
			$this->errors[] = $this->upload_errors[$file['error']]; 
			$this->succeeded = "no"; 
			return false; 
		}else{ 
			$this->file_name = pathinfo($file['name'],PATHINFO_FILENAME); 
			$this->file_extension = pathinfo($file['name'],PATHINFO_EXTENSION); 
			$this->file_type = $file['type']; 
			$this->file_size = $file['size']; 
			$this->temp_path = $file['tmp_name']; 
			$this->error = $file['error']; 
			$this->uploading_file(); 
		} 
	} 
	//uploading file 
	private function uploading_file(){ 
		//if check if extenstion valid 
		if(!in_array($this->file_extension, $this->valid_type)) { 
			$this->errors[] = "Not Valid File"; 
			$this->succeeded = "no"; 
			return false; 
		}elseif($this->file_size > $this->allowed_size){ 
			//check if size valid 
			$this->errors[] = "Not Valid size"; 
			$this->succeeded = "no"; 
			return false;			 
		}elseif(file_exists($this->upload_dir.$this->file_name.'.'.$this->file_extension)){ 
			//check file exist 
			//change file name 
			$rand = date("Y-m-dhis"); 
			//change file name 
			$this->new_file_name = $this->file_name.'_'.$rand.'.'.$this->file_extension; 
			$this->file_location = $this->upload_dir.$this->new_file_name; 
			move_uploaded_file($this->temp_path, $this->file_location); 
			$this->succeeded = "yes"; 
			return true;	 
		}else{ 
			//upload 
			$file_fixed = preg_replace('/\s+/', '_',$this->file_name); 
			$this->new_file_name = $file_fixed.'.'.$this->file_extension; 
			$this->file_location = $this->upload_dir.$this->new_file_name; 
			move_uploaded_file($this->temp_path, $this->file_location); 
			$this->succeeded = "yes"; 
			return true; 
		} 
	} 
} 
?>