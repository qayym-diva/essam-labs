<?php
require_once 'CRUD.php';
class Advertisements extends CRUD{
   //calss attributes
   public $id;
   public $image_cover; 
   public $node_id;
   public $status;
  
   //relation table attribute
    public $title; 
    public $content; 
    public $lang_id;
	public $page_alias;
	
   //push attributes for relational tables
   public function enable_relation(){
		array_push(static::$primary_fields , 'title','content','lang_id','page_alias');
   }   
   //define table name and fields
	protected static $table_name = 'advertisements';
	protected static $primary_fields = array('id','image_cover','node_id','status');
    public function adv_data($sort_filed = null, $order_by = null, $id = null,$limit = null,$lang = null){
		$sql = "SELECT advertisements.id AS id, advertisement_content.title AS title, advertisement_content.content AS content,
		        advertisements.image_cover AS image_cover,advertisement_content.lang_id as lang_id,
				advertisements.node_id AS node_id,nodes_content.alias AS page_alias,advertisements.status as status
				FROM advertisements
				LEFT JOIN advertisement_content ON advertisements.id = advertisement_content.adv_id
				LEFT JOIN nodes_content ON advertisements.node_id = nodes_content.node_id
				WHERE nodes_content.lang_id =  advertisement_content.lang_id ";
				if(!empty($lang)){
					$sql .="  AND advertisement_content.lang_id = $lang";
				}		
				
		if(!empty($id)){		
			 $sql .= " AND advertisements.id = $id ";
			 $result_array = static::find_by_sql($sql);
			 return !empty($result_array)? array_shift($result_array) : false;
		}else{		
			if(!empty($sort_filed) && !empty($order_by)){
				$sql .= " ORDER BY ".$sort_filed." ".$order_by; 
			 }
			if(!empty($limit)){
				$sql .=" limit $limit";
			}
			return self::find_by_sql($sql);  
		}				
	
	} 
	public function adv_front_page($sort_filed = null, $order_by = null,$limit = null,$lang = null){
		$sql = "SELECT advertisements.id AS id, advertisement_content.title AS title, advertisement_content.content AS content,
		        advertisements.image_cover AS image_cover,advertisement_content.lang_id as lang_id,
				pages_content.alias AS page_alias
				FROM advertisements
				LEFT JOIN advertisement_content ON advertisements.id = advertisement_content.adv_id
				LEFT JOIN pages_content ON advertisements.node_id = pages_content.node_id
				WHERE pages_content.lang_id =  advertisement_content.lang_id";
				if(!empty($lang)){
					$sql .="  AND advertisement_content.lang_id = $lang";
				}
				if(!empty($sort_filed) && !empty($order_by)){
				  $sql .= " ORDER BY ".$sort_filed." ".$order_by; 
			    }
				if(!empty($limit)){
					$sql .=" limit $limit";
				}
				return self::find_by_sql($sql);
	       }
	
	
	
   
}
?>
